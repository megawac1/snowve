
-- Author: EnormousApplePie & Lek10

include("PlotIterators")
include("FLuaVector.lua")
include("snowveutilities.lua")

--=================================================================================================================
--=================================================================================================================
--									UA					UB				UU			
--=================================================================================================================
--=================================================================================================================

-- india UA for faith at theo

local iCiv = GameInfoTypes["CIVILIZATION_INDIA"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
if bIsActive then
	GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
		print("working: india ontechbonus")
		for playerID, player in pairs(Players) do
			local player = Players[playerID];
			if player:GetCivilizationType() == iCiv then
				if player:GetTeam() == iTeam then 
					if (iTech == GameInfoTypes["TECH_THEOLOGY"]) then 
						local pCity = player:GetCapitalCity();
							pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_INDIA_TRAIT_2"], 1);
					end
				end
			end
		end
	end);
end

-- madagascar faith from hills

local iCiv = GameInfoTypes["CIVILIZATION_MALAGASY"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
if bIsActive then
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	print("working: malagasy ontechbonus")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == iCiv then
			if player:GetTeam() == iTeam then 
				if (iTech == GameInfoTypes["TECH_THEOLOGY"]) then 
					local pCity = player:GetCapitalCity();
					for pCity in player:Cities() do
						pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_MALAGASY"], 1);
					end
				end
			end
		end
	end
end);

GameEvents.PlayerCityFounded.Add(function(playerID, iX, iY)
    local pPlayer = Players[playerID]
    if pPlayer:GetCivilizationType() == iCiv and pPlayer:IsAlive() and Teams[pPlayer:GetTeam()]:IsHasTech(GameInfoTypes["TECH_THEOLOGY"]) then
        local pPlot = Map.GetPlot(iX, iY)
        local pCity = pPlot:GetPlotCity()
		pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_MALAGASY"], 1);
    end
end
)
end

-- india ua for cow spawn

local iCiv = GameInfoTypes["CIVILIZATION_INDIA"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
if bIsActive then
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	print("working: india ontechbonus")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == iCiv then
			if player:GetTeam() == iTeam then 
				if (iTech == GameInfoTypes["TECH_CHIVALRY"]) then 
				local pCity = player:GetCapitalCity();
					for pCity in player:Cities() do
						pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_INDIA_TRAIT_3"], 1);
					end
				end
			end
		end
	end
end);
end

-- norway UA
local iCiv = GameInfoTypes["CIVILIZATION_NORSK"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
local tundraHammersBuilding = GameInfoTypes["BUILDING_TUNDRA_HAMMERS"]
local philoTech = GameInfoTypes["TECH_PHILOSOPHY"]
if bIsActive then
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == iCiv then
			if player:GetTeam() == iTeam then 
				if (iTech == philoTech) then 
					local pCity = player:GetCapitalCity();
					for pCity in player:Cities() do
						pCity:SetNumRealBuilding(tundraHammersBuilding, 1);
					end
				end
			end
		end
	end
end)

GameEvents.PlayerCityFounded.Add(function(playerID, iX, iY)
    local pPlayer = Players[playerID]
    if pPlayer:GetCivilizationType() == iCiv and pPlayer:IsAlive() and Teams[pPlayer:GetTeam()]:IsHasTech(philoTech) then
        local pPlot = Map.GetPlot(iX, iY)
        local pCity = pPlot:GetPlotCity()
		pCity:SetNumRealBuilding(tundraHammersBuilding, 1);
    end
end
)
end

-- jerusalem UA
local iCiv = GameInfoTypes["CIVILIZATION_JER"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
if bIsActive then
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == iCiv then
			if player:GetTeam() == iTeam then 
				if (iTech == GameInfoTypes["TECH_PHILOSOPHY"]) then 
					local pCity = player:GetCapitalCity();
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_FUCK"], 1);
				end
			end
		end
	end
end);
end

-- chile UA


local iCiv = GameInfoTypes["CIVILIZATION_CHILE"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
if bIsActive then
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	print("working: chile ontechbonus")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_CHILE"] then
			if player:GetTeam() == iTeam then
				if (iTech == GameInfoTypes["TECH_OPTICS"]) then
					local pCity = player:GetCapitalCity();
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_CHILE_TRAIT"], 1);
				end
			end
		end
	end
end);
end

-- Philippine ua
local iCiv = GameInfoTypes["CIVILIZATION_CB_AGUINALDOPH"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

local iBuilding = GameInfoTypes["BUILDING_PHILIPPINES_TRAIT"]
local iNumCities = 2

function PhilippineExpandsBonus(playerID, iX, iY) -- Thanks Chrisy for always fixing my lua
    local pPlayer = Players[playerID]
    if pPlayer:GetCivilizationType() == GameInfoTypes["CIVILIZATION_CB_AGUINALDOPH"] and pPlayer:IsAlive() and pPlayer:CountNumBuildings(iBuilding) < iNumCities then
        local pPlot = Map.GetPlot(iX, iY)
        local pCity = pPlot:GetPlotCity()
        if not pCity:IsCapital() then
            pCity:SetNumRealBuilding(iBuilding, 1)
        end
    end
end




-- From JFD
function PhilippineMovementBonus(playerID, unitID, unitX, unitY)
	local player = Players[playerID]
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_CB_AGUINALDOPH"] and player:IsAlive() then
		local inFriendlyTerritory = false
		local unit = player:GetUnitByID(unitID)
		if not unit:IsDead() then
			if unit ~= nil then
				if (unit:GetUnitClassType() == GameInfoTypes["UNITCLASS_WORKER"] or unit:GetUnitClassType() == GameInfoTypes["UNITCLASS_SETTLER"] or unit:GetDomainType() == GameInfoTypes["DOMAIN_SEA"]) then
					if Map.GetPlot(unit:GetX(), unit:GetY()):GetOwner() == playerID then
						inFriendlyTerritory = true
					end
				end
			end
		end
		
		if inFriendlyTerritory then
			if not unit:IsHasPromotion(GameInfoTypes["PROMOTION_GOOD_FIGHT"]) then
				unit:SetHasPromotion(GameInfoTypes["PROMOTION_GOOD_FIGHT"], true)
			end
		else
			if unit:IsHasPromotion(GameInfoTypes["PROMOTION_GOOD_FIGHT"]) then
				unit:SetHasPromotion(GameInfoTypes["PROMOTION_GOOD_FIGHT"], false)
			end
		end
	end
end
if bIsActive then
GameEvents.UnitSetXY.Add(PhilippineMovementBonus) 
GameEvents.PlayerCityFounded.Add(PhilippineExpandsBonus)
end
-- Nabatea UU
local iCiv = GameInfoTypes["CIVILIZATION_MC_NABATEA"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
local iZabonah = GameInfoTypes.UNIT_MC_ZABONAH

-- Nabatea UB

function JFD_TombProduction(playerID)
	local player = Players[playerID]
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_NABATEA"] and player:IsAlive() then
		for city in player:Cities() do
			if city:IsHasBuilding(GameInfoTypes["BUILDING_MC_KOKH"]) then
				local numDomesticRoutes = JFD_GetNumDomesticRoutesFromThisCity(player, city)
				if city and city:IsHasBuilding(GameInfoTypes["BUILDING_MC_KOKH"]) then
					city:SetNumRealBuilding(GameInfoTypes["BUILDING_NABATEA_TRAIT"], numDomesticRoutes) 
				end
			end
		end
	end
end

if bIsActive then
	GameEvents.PlayerDoTurn.Add(JFD_TombProduction)
end

---- Portogual UB

local iCiv = GameInfoTypes["CIVILIZATION_PORTUGAL"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

function PortoIndiaGold(playerID)
	local player = Players[playerID]
	print('working portugal trait')
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_PORTUGAL"] and player:IsAlive() then
		for city in player:Cities() do
			if city and city:IsHasBuilding(GameInfoTypes["BUILDING_PORTO_INDIA"]) then
				local numBuildings = city:GetNumBuilding(GameInfoTypes["BUILDING_PORTO_TRAIT"])
				print('adding porto trait building number', numBuildings)
				city:SetNumRealBuilding(GameInfoTypes["BUILDING_PORTO_TRAIT"], numBuildings + 1) 
			end
		end
	end
end

if bIsActive then
	GameEvents.PlayerDoTurn.Add(PortoIndiaGold)
end

--- Moors ua

local iCiv = GameInfoTypes["CIVILIZATION_MC_LITE_MOOR"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

function MoorsEraUA(playerID)

	for playerID, player in pairs(Players) do
			local player = Players[playerID];
			local pCity = player:GetCapitalCity();
			if pCity ~= nil then
				if (player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_LITE_MOOR"] and player:GetCurrentEra() == GameInfoTypes["ERA_MEDIEVAL"]) then
					
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_MOORS_TRAIT"], 1);
					for pCity in player:Cities() do
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_MOORS_TRAIT"], 1);
					end
				else
					
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_MOORS_TRAIT"], 0);
					for pCity in player:Cities() do
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_MOORS_TRAIT"], 0);
					end
				end
			end
	end
end
function MoorsEraUA2(playerID)

	for playerID, player in pairs(Players) do
			local player = Players[playerID];
			local pCity = player:GetCapitalCity();
			if pCity ~= nil then
				if (player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_LITE_MOOR"] and player:GetCurrentEra() == GameInfoTypes["ERA_RENAISSANCE"]) then
					
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_MOORS_TRAIT2"], 1);
					for pCity in player:Cities() do
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_MOORS_TRAIT2"], 1);
					end
				else
					
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_MOORS_TRAIT2"], 0);
					for pCity in player:Cities() do
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_MOORS_TRAIT2"], 0);
					end
				end
			end
	end
end


if bIsActive then
GameEvents.PlayerDoTurn.Add(MoorsEraUA2)
GameEvents.PlayerDoTurn.Add(MoorsEraUA)
end
--- UAE UA


local iCiv = GameInfoTypes["CIVILIZATION_UAE"]
local bIsActive = JFD_IsCivilisationActive(iCiv)


-- From Sukritacts Nabatea's Civilization


--Sukritacts Nabatea's Civilization: Gold from Trade Routes

function ZabonahRouteGold(iPlayer)
	local pPlayer = Players[iPlayer]
	if pPlayer:GetCivilizationType() == GameInfoTypes["CIVILIZATION_UAE"] then
		for pUnit in pPlayer:Units() do
		

				--Gold if on Trade Route
				local pPlot = pUnit:GetPlot()
				local tTradeRoutes = pPlayer:GetInternationalTradeRoutePlotToolTip(pPlot);
				local bIsRoute = #tTradeRoutes > 0

				if (bIsRoute and pUnit:GetCombatLimit() > 0) then
					pPlayer:ChangeGold(3)
					pUnit:ChangeExperience(1)

					-- Fancy Notification
					if iPlayer == Game.GetActivePlayer() then
						if pPlayer:GetCivilizationType() == GameInfoTypes["CIVILIZATION_UAE"] then
							local iX = pPlot:GetX()
							local iY = pPlot:GetY()
							local pHexPos = ToHexFromGrid{x=iX, y=iY}
							local pWorldPos = HexToWorld(pHexPos)

							Events.AddPopupTextEvent(pWorldPos, "[COLOR_YIELD_GOLD]+3 [ICON_GOLD] Gold[ENDCOLOR]")
						end
					end
				end
		end
		
	end
end


local tClasses = {}
tClasses[GameInfoTypes.UNITCLASS_CARGO_SHIP] = GameInfoTypes.UNITCLASS_CARGO_SHIP

function DetectPlunder(iPlayer, iUnit)
    local pPlayer = Players[iPlayer]
    local pUnit = pPlayer:GetUnitByID(iUnit)
    local iUnitClass = pUnit:GetUnitClassType()
    
	if pPlayer:GetCivilizationType() == GameInfoTypes["CIVILIZATION_UAE"] then
		if tClasses[iUnitClass] ~= nil then
			local pTeam = Teams[pPlayer:GetTeam()]
			local pPlot = pUnit:GetPlot()
			local iNumUnits = pPlot:GetNumUnits()
			for iVal = 0,(iNumUnits - 1) do
				local pLUnit = pPlot:GetUnit(iVal)
				if pLUnit:GetCombatLimit() > 0 and pTeam:IsAtWar(pLUnit:GetTeam()) then
					-- Being plundered, run function
					QasimiPillageGold(Players[pLUnit:GetOwner()])
					break
				end
			end
		end
	end
function QasimiPillageGold(player)
	local goldPillaged = 200
	player:ChangeGold(goldPillaged)
end
    
    return false
end


function UAEWonderBuilt(playerID, cityID, buildingID)
	local player = Players[playerID]
	local cultureBuilding = GameInfoTypes["BUILDING_DUMMY_CULTURE"];
	if player:IsAlive() and player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_UAE"] then 
    local city = player:GetCityByID(cityID)
		if (isNationalWonder(buildingID)) then
			city:ChangeWeLoveTheKingDayCounter(6)
			local iNumToSet = city:GetNumBuilding(cultureBuilding)+3
			city:SetNumRealBuilding(cultureBuilding, iNumToSet)
		end
	end
end

if bIsActive then
Events.UnitSelectionChanged.Add(ZabonahRouteHighlight)
GameEvents.PlayerDoTurn.Add(ZabonahRouteGold)
GameEvents.CityConstructed.Add(UAEWonderBuilt)
GameEvents.CanSaveUnit.Add(DetectPlunder)
end

--- Oman UA

-- JFD_DomesticTRGold


local iCiv = GameInfoTypes["CIVILIZATION_MC_OMAN"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

--- Oman UB

local direction_types = {
	DirectionTypes.DIRECTION_NORTHEAST,
	DirectionTypes.DIRECTION_NORTHWEST,
	DirectionTypes.DIRECTION_EAST,
	DirectionTypes.DIRECTION_SOUTHEAST,
	DirectionTypes.DIRECTION_SOUTHWEST,
	DirectionTypes.DIRECTION_WEST,
	}

function JFD_MinaaAttrition(playerID)
	local player = Players[playerID]
	if player:IsAlive() and player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_OMAN"] then
		for city in player:Cities() do
			if city and city:IsHasBuilding(GameInfoTypes["BUILDING_MC_OMANI_MINAA"]) then
				local plot = Map.GetPlot(city:GetX(), city:GetY())
				for loop, direction in ipairs(direction_types) do
					local adjPlot = Map.PlotDirection(plot:GetX(), plot:GetY(), direction)
					if adjPlot:GetUnit() and adjPlot:GetUnit():GetOwner() ~= playerID then
						local unit = adjPlot:GetUnit()
						local unitOwner = Players[adjPlot:GetUnit():GetOwner()]
						if Teams[player:GetTeam()]:IsAtWar(unitOwner:GetTeam()) and unit:GetUnitCombatType() == GameInfoTypes["UNITCOMBAT_NAVALRANGED"] or unit:GetUnitCombatType() == GameInfoTypes["UNITCOMBAT_NAVALMELEE"] or unit:GetUnitCombatType() == GameInfoTypes["UNITCOMBAT_SUBMARINE"] or unit:GetUnitCombatType() == GameInfoTypes["UNITCOMBAT_CARRIER"] then
							unit:ChangeDamage(30)
						end
					end
				end
			end
		end
	end
end
if bIsActive then
GameEvents.PlayerDoTurn.Add(JFD_MinaaAttrition)
end



function JFD_MinaaNavalProduction(playerID)
	local player = Players[playerID]
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_OMAN"] and player:IsAlive() then
		for city in player:Cities() do
			if city:IsHasBuilding(GameInfoTypes["BUILDING_MC_OMANI_MINAA"]) then
				local numDomesticRoutes = JFD_GetNumDomesticRoutesFromThisCity(player, city)
				if city and city:IsHasBuilding(GameInfoTypes["BUILDING_MC_OMANI_MINAA"]) then
					city:SetNumRealBuilding(GameInfoTypes["BUILDING_OMAN_TRAIT"], numDomesticRoutes) 
				end
			end
		end
	end
end

if bIsActive then
	GameEvents.PlayerDoTurn.Add(JFD_MinaaNavalProduction)	
end

--- Cuba ua

local iCiv = GameInfoTypes["CIVILIZATION_UC_CUBA_BATISTA"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

-- UB: Dance Hall

function DanceHallDo(iPlayer)
    local pPlayer = Players[iPlayer]
    if pPlayer:GetCivilizationType() == GameInfoTypes["CIVILIZATION_UC_CUBA_BATISTA"] then
        for pCity in pPlayer:Cities() do
            if pCity:IsHasBuilding(GameInfoTypes.BUILDING_DANCE_HALL) then
                if pCity:GetNumGreatWorksInBuilding(GameInfoTypes.BUILDINGCLASS_OPERA_HOUSE) > 0 then
                    pCity:SetNumRealBuilding(GameInfoTypes.BUILDING_CUBA_TRAIT_UB, 1)
                else
                    pCity:SetNumRealBuilding(GameInfoTypes.BUILDING_CUBA_TRAIT_UB, 0)
                end
            end
        end
    end
end

-- UA
function CubaCultureYoink(iPlayer) --- Fixed my original broken code by LeeS(Master of the Galaxy)
    local player = Players[iPlayer]
    if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_UC_CUBA_BATISTA"] then
        local capital = player:GetCapitalCity()
        if (capital ~= nil) then
            local iNumToSet = 0
            for i = 0, GameDefines.MAX_MAJOR_CIVS - 1, 1 do
                if (iPlayer ~= i) then
                    local pOtPlayer = Players[i]
                    if (pOtPlayer ~= nil) and pOtPlayer:IsEverAlive() and pOtPlayer:IsAlive() and (pOtPlayer:GetCapitalCity() ~= nil) and player:GetTeam() == pOtPlayer:GetTeam() then
                        print("has met a civ, begin stealing their culture...")
                        local otherCapital = pOtPlayer:GetCapitalCity()
                        iNumToSet = (iNumToSet +  math.floor(otherCapital:GetBaseJONSCulturePerTurn() * .34) )
                    end
                end
            end
            capital:SetNumRealBuilding(GameInfoTypes.BUILDING_DUMMY_CULTURE, iNumToSet)
        end
    end
end

if bIsActive then
	 GameEvents.PlayerDoTurn.Add(DanceHallDo)
	 GameEvents.PlayerDoTurn.Add(CubaCultureYoink)
end

-- bolivia UA. Couldn't bother with the button UI, too much of a time sink ~EAP
local iCiv = GameInfoTypes["CIVILIZATION_LEU_BOLIVIA_BELZU"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

function IsPersonExpended(iPlayer, iUnit)
	print("working: Bolivia")
	
	local pPlayer = Players[iPlayer];
	local pCity = pPlayer:GetCapitalCity();
    if (pPlayer:IsEverAlive()) then
		print("found a civ")
        if pPlayer:GetCivilizationType() == GameInfoTypes["CIVILIZATION_LEU_BOLIVIA_BELZU"] then
			print("found Bolivia")
			local ArtUnitID = GameInfoTypes["UNIT_ARTIST"]
			local iMusician = GameInfoTypes["UNIT_LEU_COMPARSA_FOLKLORICA"]
			if (iUnit == ArtUnitID or iUnit == iMusician) then
				print("found the Unit -> Artist or Musician")
				pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_BOLIVIA_TRAIT_PRODUCTION"], 1);
				pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_BOLIVIA_TRAIT_FOOD"], 0);
				if pPlayer:IsHuman() and pPlayer:GetCivilizationType() == iCiv and Game.GetActivePlayer() == iPlayer then
				Events.GameplayAlertMessage(Locale.ConvertTextKey("TXT_KEY_THP_BOLIVIA_BUTTON_TITLE_RIGHT"))
				end
			end
			local iWriter = GameInfoTypes["UNIT_WRITER"]
			if (iUnit == iWriter) then
				print("found the Unit -> Writer")
				pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_BOLIVIA_TRAIT_FOOD"], 1);
				pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_BOLIVIA_TRAIT_PRODUCTION"], 0);
				if (pPlayer:IsHuman() and pPlayer:GetCivilizationType() == iCiv and Game.GetActivePlayer() == iPlayer) then
				Events.GameplayAlertMessage(Locale.ConvertTextKey("TXT_KEY_THP_BOLIVIA_BUTTON_TITLE_LEFT"))
				end
			end
		end
	end
end


local function JFD_BoliviaBelzu_PlayerDoTurn(playerID)
	
	local player = Players[playerID]
--COLORADO -- From JDF/Leugi
	local unitColoradoID			= GameInfoTypes["UNIT_COLORADO"]
	local unitColoradoStrengthBase	= GameInfo.Units[unitColoradoID].Combat
	local numHappiness = player:GetExcessHappiness()
	local numBonusCombatStrength = math.min(Game_GetRound(numHappiness/5))
	local unitColoradoStrength = 40
	if (numBonusCombatStrength > 0 ) then
		unitColoradoStrength = unitColoradoStrengthBase + (numBonusCombatStrength * 2)
	end	
	for unit in player:Units() do
		if unit:GetUnitType() == unitColoradoID then
			unit:SetBaseCombatStrength(unitColoradoStrength)
		end
	end
end

if bIsActive then
	GameEvents.GreatPersonExpended.Add(IsPersonExpended)
	GameEvents.PlayerDoTurn.Add(JFD_BoliviaBelzu_PlayerDoTurn)
	GameEvents.UnitSetXY.Add(JFD_BoliviaBelzu_PlayerDoTurn)
end

-- mongolia additional UA. This is where the spice begins ~EAP

local iCiv = GameInfoTypes["CIVILIZATION_MONGOL"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

if bIsActive then
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	print("working: mongolia ontechbonus")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MONGOL"] then
			if player:GetTeam() == iTeam then
				if (iTech == GameInfoTypes["TECH_CHIVALRY"]) then
					local pCity = player:GetCapitalCity();
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_TOGTVORTOI"], 1);
					for pCity in player:Cities() do
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_TOGTVORTOI"], 1) end
				end
			end
		end
	end
end);

end

-- Romania new UA Culture from GA  (orignal code from DJS)

local iCiv = GameInfoTypes["CIVILIZATION_MC_ROMANIA"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

function RomaniaGACulture(playerID)
	local player = Players[playerID]
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_ROMANIA"] and player:IsAlive() then
		for city in player:Cities() do
			if (city:GetNumBuilding(GameInfoTypes["BUILDING_ROMANIA_TRAIT"]) > 0) then
				city:SetNumRealBuilding(GameInfoTypes["BUILDING_ROMANIA_TRAIT"], 0)
			end
			if player:IsGoldenAge() then
				city:SetNumRealBuilding(GameInfoTypes["BUILDING_ROMANIA_TRAIT"], 1)
			end
		end
	end
end
if bIsActive then
GameEvents.PlayerDoTurn.Add(RomaniaGACulture)
end

-- California UA, food carryover during Golden Age and ga points from growing
-- california silicon valley yields

local iCiv = GameInfoTypes["CIVILIZATION_CALI"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
local caliFoodBuilding = GameInfoTypes["BUILDING_CALIFORNIA_TRAIT"]
local siliconValley = GameInfoTypes["BUILDING_SILICON_VALLEY"]
local cali1 = GameInfoTypes["BUILDING_CALIFORNIA_SILICON_1"]
local cali2 = GameInfoTypes["BUILDING_CALIFORNIA_SILICON_2"]

function CaliforniaGAFood(playerID)
	local player = Players[playerID]
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_CALI"] and player:IsAlive() then
		local city = player:GetCapitalCity();
		if (city:GetNumBuilding(caliFoodBuilding) > 0) then 
			city:SetNumRealBuilding(caliFoodBuilding, 0)
		end
		if player:IsGoldenAge() then
			city:SetNumRealBuilding(caliFoodBuilding, 1)
		end
	end
end

function CaliforniaGAPoints(iX, iY, oldPopulation, newPopulation)
	local plot = Map.GetPlot(iX, iY)
	local city = plot:GetPlotCity();
	local player = Players[city:GetOwner()];
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_CALI"] and player:IsAlive() and city == player:GetCapitalCity() then
		player:ChangeGoldenAgeProgressMeter(newPopulation)
	end
end

function CaliforniaSiliconYieldsTech(iTeam, iTech, bAdopted)
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == iCiv then
			if player:GetTeam() == iTeam then
				if (iTech == GameInfoTypes["TECH_ECONOMICS"]) then
					if player:IsAlive() then
						for city in player:Cities() do
								if city:IsHasBuilding(siliconValley) then
										city:SetNumRealBuilding(cali1, 1)
								end
						end
					end
				end
				if (iTech == GameInfoTypes["TECH_COMPUTERS"]) then
					if player:IsAlive() then
						for city in player:Cities() do
								if city:IsHasBuilding(siliconValley) then
										city:SetNumRealBuilding(cali2, 1)
								end
						end
					end
				end
			end
		end
	end
end

function CaliforniaSiliconYieldsBuilt(playerID, cityID, buildingID)
	local player = Players[playerID]
	local city = player:GetCityByID(cityID)
	if buildingID == siliconValley then
		if Teams[player:GetTeam()]:IsHasTech(GameInfoTypes["TECH_ECONOMICS"]) then
			city:SetNumRealBuilding(cali1, 1)
			if Teams[player:GetTeam()]:IsHasTech(GameInfoTypes["TECH_COMPUTERS"]) then
				city:SetNumRealBuilding(cali2, 1)
			end
		end
	end
end

if bIsActive then
	GameEvents.TeamSetHasTech.Add(CaliforniaGAFood)
	GameEvents.TeamSetHasTech.Add(CaliforniaSiliconYieldsTech)
	GameEvents.CityConstructed.Add(CaliforniaSiliconYieldsBuilt)
	GameEvents.SetPopulation.Add(CaliforniaGAPoints)
end



-- Maori Promotion (UA) 
-- Code by EnormousApplePie

local iCiv = GameInfoTypes["CIVILIZATION_MC_MAORI"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

local unitPromotionStaticID	= GameInfoTypes["PROMOTION_MAORI_DULL"]
local unitPromotionActiveID	= GameInfoTypes["PROMOTION_MAORI"]
local unitPromotionActiveCivilID	= GameInfoTypes["PROMOTION_MAORI_CIVILIAN"]

function EAP_Maori_Turn(playerID)
	local player = Players[playerID]
	local playerTeam = Teams[player:GetTeam()]
	local Gameturn = Game.GetGameTurn()
	if (not player:IsAlive()) then return end
	if player:IsBarbarian() then return end
	if (not player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_MAORI"]) then return end
	for unit in player:Units() do
		local isPromotionValid = false
		if Gameturn >= 5 then 
			isPromotionValid = true
			--print("It is turn 9 or later")
		end
		if isPromotionValid then
			if unit:IsHasPromotion(unitPromotionActiveID) then
				unit:SetHasPromotion(unitPromotionActiveID, false)
			elseif unit:IsHasPromotion(unitPromotionActiveCivilID) then
				unit:SetHasPromotion(unitPromotionActiveCivilID, false)
			end
		end
	end
end
if bIsActive then
GameEvents.PlayerDoTurn.Add(EAP_Maori_Turn)
end

local unitEmbarkPromotionActiveID = GameInfoTypes["PROMOTION_MOVE_ALL_TERRAIN"]
local unitEmbarkPromotionID = GameInfoTypes["PROMOTION_EMBARKATION"]

function EAP_Embark_Fix(playerID)
	local player = Players[playerID]
	local playerTeam = Teams[player:GetTeam()]
	if (not player:IsAlive()) then return end
	if player:IsBarbarian() then return end
	for unit in player:Units() do
		if unit:IsHasPromotion(unitEmbarkPromotionActiveID) then
			unit:SetHasPromotion(unitEmbarkPromotionID, false)
		end
	end
end
GameEvents.PlayerDoTurn.Add(EAP_Embark_Fix)

--[[
-- New Zealand UA 
-- Original code by JDF

local iCiv = GameInfoTypes["CIVILIZATION_MC_NEW_ZEALAND"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
     
function NZMeetBonus(playerMetID, playerID)
			local newzciv = GameInfoTypes["CIVILIZATION_MC_NEW_ZEALAND"]
			local player = Players[playerID]
            local playerMet = Players[playerMetID]
            local rewardCulture = 12
            local rewardScience = 24
            local rewardGold = 40
            local rewardFaith = 14
			if playerMet:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_NEW_ZEALAND"] then
				for newzciv in newzciv do NZMeetBonus() end
			end

            if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_NEW_ZEALAND"] then
                    local random = GetRandom(1, 4)
                    if random == 1 then
                            player:ChangeFaith(rewardFaith)
							if player:IsHuman() and player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_NEW_ZEALAND"] and Game.GetActivePlayer() == playerID then
								Events.GameplayAlertMessage(Locale.ConvertTextKey("You receive [COLOR_POSITIVE_TEXT]{1_Num} [ICON_PEACE] Faith[ENDCOLOR] from meeting [COLOR_POSITIVE_TEXT]{2_CivName}[ENDCOLOR]", rewardFaith, playerMet:GetName()))
							end
                    elseif random == 2 then
                            player:ChangeJONSCulture(rewardCulture)
							if player:IsHuman() and player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_NEW_ZEALAND"] and Game.GetActivePlayer() == playerID then
								Events.GameplayAlertMessage(Locale.ConvertTextKey("You receive [COLOR_POSITIVE_TEXT]{1_Num} [ICON_CULTURE] Culture[ENDCOLOR] from meeting [COLOR_POSITIVE_TEXT]{2_CivName}[ENDCOLOR]", rewardCulture, playerMet:GetName()))
							end
                    elseif random == 3 then
                            Teams[player:GetTeam()]:GetTeamTechs():ChangeResearchProgress(player:GetCurrentResearch(), rewardScience, playerID)
							if player:IsHuman() and player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_NEW_ZEALAND"] and Game.GetActivePlayer() == playerID then
								Events.GameplayAlertMessage(Locale.ConvertTextKey("You receive [COLOR_POSITIVE_TEXT]{1_Num} [ICON_RESEARCH] Science[ENDCOLOR] from meeting [COLOR_POSITIVE_TEXT]{2_CivName}[ENDCOLOR]", rewardScience, playerMet:GetName()))
							end
                    else
                            player:ChangeGold(rewardGold)
							if player:IsHuman() and player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_NEW_ZEALAND"] and Game.GetActivePlayer() == playerID then
								 Events.GameplayAlertMessage(Locale.ConvertTextKey("You receive [COLOR_POSITIVE_TEXT]{1_Num} [ICON_GOLD] Gold[ENDCOLOR] from meeting [COLOR_POSITIVE_TEXT]{2_CivName}[ENDCOLOR]", rewardGold, playerMet:GetName()))
							end
                          
                    end
            end
end

-- New Zealand UU infantry Influence
-- Code by MC and JFD

include("FLuaVector.lua")
-- JFD_IsInCityStateBorders
function JFD_IsInCityStateBorders(playerID, unit)
	local plot = unit:GetPlot()
	if plot:GetOwner() > -1 then
		local player = Players[plot:GetOwner()]
		return (player:IsMinorCiv() and player:GetMinorCivFriendshipLevelWithMajor(playerID) >= 1)
	end
	return false
end

-- MC_MaoriBattalion
local unitPromotionMaoriID = GameInfoTypes["PROMOTION_ATTACK_AWAY_CAPITAL"]
function MC_MaoriBattalion(playerID)
	local player = Players[playerID]
	if (player:IsAlive() and (not player:IsBarbarian())) then	
		for unit in player:Units() do
			if unit:IsHasPromotion(unitPromotionMaoriID) then
				if JFD_IsInCityStateBorders(playerID, unit) then
					local minorPlayerID = unit:GetPlot():GetOwner()
					local minorPlayer = Players[minorPlayerID]
					minorPlayer:ChangeMinorCivFriendshipWithMajor(playerID, 1)
					if player:IsHuman() and Game.GetActivePlayer() == playerID then
						local hex = ToHexFromGrid(Vector2(unit:GetX(), unit:GetY()))
						Events.AddPopupTextEvent(HexToWorld(hex), Locale.ConvertTextKey("[ICON_WHITE]+{1_Num}[ENDCOLOR] [ICON_INFLUENCE]", 1), true)
					end
				end
			end
		end
	end
end


-- New Zealand UU Ironclad promo near Cities
-- Code by JDF

local unitPromotionDefenderActiveID	= GameInfoTypes["PROMOTION_JFD_DEFENDER_ACTIVE"]
local unitPromotionDefenderID		= GameInfoTypes["PROMOTION_JFD_DEFENDER"]
function JFD_NewZealand_Defender_PlayerDoTurn(playerID)
	local player = Players[playerID]
	local playerTeam = Teams[player:GetTeam()]
	if (not player:IsAlive()) then return end
	if player:IsBarbarian() then return end
	for unit in player:Units() do
		local isPromotionValid = false
		if (unit and (unit:IsHasPromotion(unitPromotionDefenderActiveID) or unit:IsHasPromotion(unitPromotionDefenderID))) then
			local plot = unit:GetPlot()
			if (plot and plot:IsPlayerCityRadius(playerID)) then 
				isPromotionValid = true
			else
				for otherPlayerID = 0, GameDefines.MAX_MAJOR_CIVS-1, 1 do
					local otherPlayer = Players[otherPlayerID]
					if (otherPlayer:IsAlive() and otherPlayerID ~= playerID) then	
						local otherTeamID = otherPlayer:GetTeam()
						if otherPlayer:IsDoF(player:GetTeam()) then
							if plot:IsPlayerCityRadius(otherPlayerID) then 
								isPromotionValid = true
								break
							end
						end
					end
				end
			end
		end
		if isPromotionValid then
			if (not unit:IsHasPromotion(unitPromotionDefenderActiveID)) then
				unit:SetHasPromotion(unitPromotionDefenderActiveID, true)
				unit:SetHasPromotion(unitPromotionDefenderID, false)
			end
		else
			if unit:IsHasPromotion(unitPromotionDefenderActiveID) then
				unit:SetHasPromotion(unitPromotionDefenderActiveID, false)
				unit:SetHasPromotion(unitPromotionDefenderID, true)
			end
		end
	end
end

if bIsActive then
	GameEvents.TeamMeet.Add(NZMeetBonus)
	GameEvents.PlayerDoTurn.Add(MC_MaoriBattalion)
	GameEvents.PlayerDoTurn.Add(JFD_NewZealand_Defender_PlayerDoTurn)
end --]]

	-- Tongo UB food
	-- Code by JFD
	-- Hoped to edit it somewhat and make it slightly more original, but I couldn't quite change it without bringing down the balance ~EAP
	local iCiv = GameInfoTypes["CIVILIZATION_MC_TONGA"]
	local bIsActive = JFD_IsCivilisationActive(iCiv)

	local direction_types = {
            DirectionTypes["DIRECTION_NORTHEAST"],
            DirectionTypes["DIRECTION_NORTHWEST"],
            DirectionTypes["DIRECTION_EAST"],
            DirectionTypes["DIRECTION_SOUTHEAST"],
            DirectionTypes["DIRECTION_SOUTHWEST"],
            DirectionTypes["DIRECTION_WEST"]
            }
     
    function JFD_GetNumAdjacentSeaTiles(city)
            local numAdjacentSeaTiles = 0
            if Map.GetPlot(city:GetX(), city:GetY()) then
                    for loop, direction in ipairs(direction_types) do
                            local adjPlot = Map.PlotDirection(city:GetX(), city:GetY(), direction)
                            if adjPlot:GetTerrainType() == GameInfoTypes["TERRAIN_COAST"] then     
                                    numAdjacentSeaTiles = numAdjacentSeaTiles + 1
                            end
                    end
            end
           
            return numAdjacentSeaTiles     
    end
     
    function JFD_MalaeFood(playerID, unitID, unitX, unitY)
            local player = Players[playerID]
            if player:IsAlive() then
                    for city in player:Cities() do
                            if city:IsHasBuilding(GameInfoTypes["BUILDING_MC_TONGAN_MALAE"]) and JFD_GetNumAdjacentSeaTiles(city) >= 3 then
                                    city:SetNumRealBuilding(GameInfoTypes["BUILDING_MC_MALAE_FOOD"], 1)
                            end
                    end
            end
    end

if bIsActive then
   GameEvents.PlayerDoTurn.Add(JFD_MalaeFood)
end

-- Aksum UA 
-- Ah, Optimization! Thanks JFD!
local iCiv = GameInfoTypes["CIVILIZATION_AKSUM"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

if JFD_IsCivilisationActive(iCiv) then
	print("Aksum is in this game")
end

-- Aksum UA function 
function GetTradeRoutesNumberAksum(player, city)
	local tradeRoutes = player:GetTradeRoutes()
	local numRoutesAksum = 0
	for i, v in ipairs(tradeRoutes) do
		local originatingCity = v.FromCity
		if (originatingCity == city) then
			numRoutesAksum = numRoutesAksum + 1
		end
	end
	
	return numRoutesAksum
end


local buildingTraitAksumID = GameInfoTypes["BUILDING_AKSUM_TRAIT"]
function AksumTrait(playerID)
	local player = Players[playerID]
    if player:IsEverAlive() and player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_AKSUM"] then 
		for city in player:Cities() do
			city:SetNumRealBuilding(buildingTraitAksumID, math.min(GetTradeRoutesNumberAksum(player, city), 1)) -- it does!
		end
	end
end

if bIsActive then
	GameEvents.PlayerDoTurn.Add(AksumTrait)
end

-- Byz UA 
local iCiv = GameInfoTypes["CIVILIZATION_BYZANTIUM"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

function ByzantiumGreatPeopleTrait(playerID, unitID)
	print("working byzantium trait")
	local GeneralID = GameInfoTypes["UNIT_GREAT_GENERAL"]
	local player = Players[playerID]
	local civ = player:GetCivilizationType()
	local isGeneral = (unitID == GeneralID)
	if civ == GameInfoTypes["CIVILIZATION_BYZANTIUM"] then
		if not isGeneral then
			print("Adding byz trait")
			local pCity = player:GetCapitalCity();	
			local numBuildings = pCity:GetNumBuilding(GameInfoTypes["BUILDING_BYZANTIUM_TRAIT"])
			pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_BYZANTIUM_TRAIT"], numBuildings + 1)
		end
	end
end

if bIsActive then
	GameEvents.GreatPersonExpended.Add(ByzantiumGreatPeopleTrait)
end

-- Vatican UA 
local iCiv = GameInfoTypes["CIVILIZATION_VATICAN"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

function VaticanGreatLeachTrait(playerID, unitID)
	print("working vatican trait")
	local player = Players[playerID]
	local civ = player:GetCivilizationType()
	if civ ~= GameInfoTypes["CIVILIZATION_VATICAN"] then
		for otherPlayerID, otherPlayer in pairs(Players) do
			-- Find the Vatican player on the same team
			if otherPlayer:GetCivilizationType() == GameInfoTypes["CIVILIZATION_VATICAN"] and otherPlayer:GetTeam() == player:GetTeam() then
				local buildingType = nil

				-- Determine the building type to add
				if unitID == GameInfoTypes["UNIT_PROPHET"] then
					buildingType = "BUILDING_VATICAN_TRAIT_PROPHET"
				elseif unitID == GameInfoTypes["UNIT_SCIENTIST"] then
					buildingType = "BUILDING_VATICAN_TRAIT_SCIENTIST"
				elseif unitID == GameInfoTypes["UNIT_MERCHANT"] then
					buildingType = "BUILDING_VATICAN_TRAIT_MERCHANT"
				elseif unitID == GameInfoTypes["UNIT_ENGINEER"] then
					buildingType = "BUILDING_VATICAN_TRAIT_ENGINEER"
				elseif unitID == GameInfoTypes["UNIT_WRITER"] then
					buildingType = "BUILDING_VATICAN_TRAIT_WRITER"
				elseif unitID == GameInfoTypes["UNIT_ARTIST"] or unitID == GameInfoTypes["UNIT_ITALARTIST"] then
					buildingType = "BUILDING_VATICAN_TRAIT_ARTIST"
				elseif unitID == GameInfoTypes["UNIT_MUSICIAN"] or unitID == GameInfoTypes["UNIT_LEU_COMPARSA_FOLKLORICA"] then
					buildingType = "BUILDING_VATICAN_TRAIT_MUSICIAN"
				end
			
				if buildingType ~= nil then
					print(string.format("Adding building: %s", buildingType))
					for pCity in otherPlayer:Cities() do
						local numBuildings = pCity:GetNumBuilding(GameInfoTypes[buildingType])
						pCity:SetNumRealBuilding(GameInfoTypes[buildingType], numBuildings + 1)
					end
				end
			end 
		end
	end
end

if bIsActive then
	GameEvents.GreatPersonExpended.Add(VaticanGreatLeachTrait)
end

-- Kilwa UA 
-- Ah, Optimization! Thanks JFD!
local iCiv = GameInfoTypes["CIVILIZATION_MC_KILWA"]
local bIsActive = JFD_IsCivilisationActive(iCiv)


-- Kilwa UA function 
function GetTradeRoutesNumber(player, city)
	print("working: kilwa 1")
	local tradeRoutes = player:GetTradeRoutes()
	local numRoutes = 0
	for i, v in ipairs(tradeRoutes) do
		local originatingCity = v.FromCity
		if (originatingCity == city) and (v.FromID ~= v.ToID) then -- Thnx TophatPalading for fixing my stuff <3 
			numRoutes = numRoutes + 1
		end
	end
	
	return numRoutes
end


local buildingCoralPortID = GameInfoTypes["BUILDING_CORALSHOP"]
local buildingTraitKilwaID = GameInfoTypes["BUILDING_KILWA_TRAIT"]
function KilwaTrait(playerID)
	print("working: kilwa 2")
	local player = Players[playerID]
    if player:IsEverAlive() and player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_KILWA"] then 
		for city in player:Cities() do
			city:SetNumRealBuilding(buildingTraitKilwaID, math.min(GetTradeRoutesNumber(player, city), 420)) -- I wonder if this will work (note: it does)
		end
	end
end

if bIsActive then
	GameEvents.PlayerDoTurn.Add(KilwaTrait)
end

-- Ukraine UA 

local iCiv = GameInfoTypes["CIVILIZATION_UKRAINE"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
if bIsActive then
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	print("working: ukraine ontechbonus")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_UKRAINE"] then
			if player:GetTeam() == iTeam then
				if (iTech == GameInfoTypes["TECH_ARCHERY"]) then
					local pCity = player:GetCapitalCity();
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_UKRAINE_TRAIT"], 1);
				end
			end
		end
	end
end);
end
-- Turks Unit Promotion 

include("PlotIterators")
local civID = GameInfoTypes["CIVILIZATION_UC_TURKEY"]
local promoID = GameInfoTypes["PROMOTION_UNIT_TURKISH_GWI"]
function TurksGeneralreset(playerID)
	local player = Players[playerID]
	if player:GetCivilizationType() == civID then
		local greatGenerals = {}
		for unit in player:Units() do
			if unit:IsHasPromotion(promoID) then
				unit:SetHasPromotion(promoID, false)
			end
			if unit:IsHasPromotion(GameInfoTypes["PROMOTION_GREAT_GENERAL"]) then
				table.insert(greatGenerals, unit)
			end
		end
		for key,greatGeneral in pairs(greatGenerals) do 
			local plot = greatGeneral:GetPlot()
			for loopPlot in PlotAreaSweepIterator(plot, 2, SECTOR_NORTH, DIRECTION_CLOCKWISE, DIRECTION_OUTWARDS, CENTRE_INCLUDE) do
				for i = 0, loopPlot:GetNumUnits() - 1, 1 do  
					local otherUnit = loopPlot:GetUnit(i)
					if otherUnit and otherUnit:GetOwner() == playerID and otherUnit:IsCombatUnit() then
						otherUnit:SetHasPromotion(promoID, true)
					end
				end
			end
		end
	end
end
GameEvents.PlayerDoTurn.Add(TurksGeneralreset)

function TurksGeneralbonus(playerID, unitID, unitX, unitY)
	local player = Players[playerID]
	if player:GetCivilizationType() == civID and (player:GetUnitClassCount(GameInfoTypes["UNITCLASS_GREAT_GENERAL"]) > 0) then
		local unit = player:GetUnitByID(unitID)
		local plot = unit:GetPlot()
		if unit:IsHasPromotion(GameInfoTypes["PROMOTION_GREAT_GENERAL"]) then
			for loopPlot in PlotAreaSweepIterator(plot, 2, SECTOR_NORTH, DIRECTION_CLOCKWISE, DIRECTION_OUTWARDS, CENTRE_INCLUDE) do
				for i = 0, loopPlot:GetNumUnits() - 1, 1 do  
					local otherUnit = loopPlot:GetUnit(i)
					if otherUnit and otherUnit:GetOwner() == playerID and otherUnit:IsCombatUnit() and unit:IsHasPromotion(GameInfoTypes.PROMOTION_FREE_UPGRADE_TURKISH) then
						otherUnit:SetHasPromotion(promoID, true)
					end
				end
			end
		elseif unit:IsCombatUnit() and unit:IsHasPromotion(GameInfoTypes.PROMOTION_FREE_UPGRADE_TURKISH) then
			unit:SetHasPromotion(promoID, false)
			for loopPlot in PlotAreaSweepIterator(plot, 2, SECTOR_NORTH, DIRECTION_CLOCKWISE, DIRECTION_OUTWARDS, CENTRE_INCLUDE) do
				for i = 0, loopPlot:GetNumUnits() - 1, 1 do  
					local otherUnit = loopPlot:GetUnit(i)
					if otherUnit and otherUnit:GetOwner() == playerID and otherUnit:IsHasPromotion(GameInfoTypes["PROMOTION_GREAT_GENERAL"]) then
						unit:SetHasPromotion(promoID, true)
					end
				end
			end
		end
	end
end
GameEvents.UnitSetXY.Add(TurksGeneralbonus)

-- Ottoman new UA addition
-- Code by Uighur_Caesar

include("FLuaVector.lua")

local ottoID = GameInfoTypes.CIVILIZATION_OTTOMAN

function OttoPromotion(ownerId, unitId, ePromotion)
	local player = Players[ownerId]
	if player:IsAlive() and player:GetCivilizationType() == ottoID then
		local unit = player:GetUnitByID(unitId)
		local xp = unit:GetExperience()
		local needed = unit:ExperienceNeeded()
		local faith = math.ceil(needed / 3)
		player:ChangeFaith(faith)
		if player:IsHuman() and Game.GetActivePlayer() == ownerId then
			local hex = ToHexFromGrid(Vector2(unit:GetX(), unit:GetY()))
			Events.AddPopupTextEvent(HexToWorld(hex), Locale.ConvertTextKey("+{1_Num}[ENDCOLOR] [ICON_PEACE]", faith), true)
		end
	end
end

function OttoCityBuiltBuilding(playerID, cityID)
	local player = Players[playerID]
	if player:GetCivilizationType() == ottoID then
		local city = player:GetCityByID(cityID)
		city:ChangeFood(5)
	end
end

GameEvents.CityConstructed.Add(OttoCityBuiltBuilding)
GameEvents.UnitPromoted.Add(OttoPromotion)



-- Tibet new Trait UA

local iCiv = GameInfoTypes["CIVILIZATION_TIBET"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

if bIsActive then
print("loaded tibet ua")
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	print("working: tibet UA 25%")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_TIBET"] then
			if player:GetTeam() == iTeam then
				if (iTech == GameInfoTypes["TECH_DRAMA"]) then
					local pCity = player:GetCapitalCity();
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_TIBET"], 1);
				end
			end
		end
	end
end)
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	print("working: tibet UA 50%")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_TIBET"] then
			if player:GetTeam() == iTeam then
				if (iTech == GameInfoTypes["TECH_ACOUSTICS"]) then
					local pCity = player:GetCapitalCity();
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_TIBET2"], 1);
				end
			end
		end
	end
end)
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	print("working: tibet UA 75%")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_TIBET"] then
			if player:GetTeam() == iTeam then
				if (iTech == GameInfoTypes["TECH_RADIO"]) then
					local pCity = player:GetCapitalCity();
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_TIBET3"], 1);
				end
			end
		end
	end
end)
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	print("working: tibet UA 100%")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_TIBET"] then
			if player:GetTeam() == iTeam then
				if (iTech == GameInfoTypes["TECH_GLOBALIZATION"]) then
					local pCity = player:GetCapitalCity();
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_TIBET4"], 1);
				end
			end
		end
	end
end)
end


local iCiv = GameInfoTypes["CIVILIZATION_VENEZ"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

-- Venez trait tech research
print("loaded venez ua")
if bIsActive then
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	print("working: venez UA")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_VENEZ"] then
			if player:GetTeam() == iTeam then
				if (iTech == GameInfoTypes["TECH_COMPASS"]) then
					local pCity = player:GetCapitalCity();
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_VENEZ_TRAIT"], 1);
				end
			end
		end
	end
end)
end
-- Horde free building 

local iCiv = GameInfoTypes["CIVILIZATION_HORDE"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

if bIsActive then
print("loaded horde ua")
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	print("working: horde UA")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_HORDE"] then
			if player:GetTeam() == iTeam then
				if (iTech == GameInfoTypes["TECH_PHILOSOPHY"]) then
					local pCity = player:GetCapitalCity();
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_JARLIQ"], 1);
				end
			end
		end
	end
end)
end

-- korea ua to spawn buildings in cap based on national wonder tech (fix later I guess)

local iCiv = GameInfoTypes["CIVILIZATION_KOREA"]
local koreaIsActive = JFD_IsCivilisationActive(iCiv)

function KoreaUA(iTeam, iTech, bAdopted)
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_KOREA"] and player:IsAlive() then
			if player:GetTeam() == iTeam then 
				local pCity = player:GetCapitalCity();
				if (iTech == GameInfoTypes["TECH_DRAMA"]) then 
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_MONUMENT"], 1)
				elseif (iTech == GameInfoTypes["TECH_PHILOSOPHY"]) then 
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_LIBRARY"], 1)
				elseif (iTech == GameInfoTypes["TECH_BRONZE_WORKING"]) then 
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_BARRACKS"], 1)
				elseif (iTech == GameInfoTypes["TECH_THEOLOGY"]) then
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_TEMPLE"], 1)
				elseif (iTech == GameInfoTypes["TECH_MACHINERY"]) then 
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_WORKSHOP"], 1)
				elseif (iTech == GameInfoTypes["TECH_EDUCATION"]) then
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_UNIVERSITY"], 1)
				elseif (iTech == GameInfoTypes["TECH_INDUSTRIALIZATION"]) then
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_FACTORY"], 1)
				elseif (iTech == GameInfoTypes["TECH_GUILDS"]) then
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_MARKET"], 1)
				elseif (iTech == GameInfoTypes["TECH_ARCHITECTURE"]) then
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_OPERA_HOUSE"], 1)
				elseif (iTech == GameInfoTypes["TECH_HORSEBACK_RIDING"]) then
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_COLOSSEUM"], 1)
				end
			end
		end
	end
end

if koreaIsActive then
	GameEvents.TeamSetHasTech.Add(KoreaUA)
end 

-- Mysore free building 

local iCiv = GameInfoTypes["CIVILIZATION_MYSORE"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

if bIsActive then
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MYSORE"] then
			if player:GetTeam() == iTeam then
				if (iTech == GameInfoTypes["TECH_MASONRY"]) then
					local pCity = player:GetCapitalCity();
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_MYSORE_WALLS"], 1);
				end
			end
		end
	end
end)
end

-- france free building
local iCiv = GameInfoTypes["CIVILIZATION_FRANCE"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

if bIsActive then
GameEvents.TeamSetHasTech.Add(function(iTeam, iTech, bAdopted)
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_FRANCE"] then
			if player:GetTeam() == iTeam then
				if (iTech == GameInfoTypes["TECH_WRITING"]) then
					local pCity = player:GetCapitalCity();
					pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_LIGHT"], 1);
				end
			end
		end
	end
end)
end

----------------------------------------------------------
-------------------- DUMMY POLICIES ----------------------
----------------------------------------------------------

-- 'Nam dummy policy

local iCiv = GameInfoTypes["CIVILIZATION_JURCHEN"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

print("dummy policy loaded - Manchuria")
function DummyPolicy(player)
	print("working - Manchuria")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_JURCHEN"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_MANCHURIA"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_MANCHURIA"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end

-- khmer dummy policy

local iCiv = GameInfoTypes["CIVILIZATION_KHMER"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

print("dummy policy loaded - Khmer")
function DummyPolicy(player)
	print("working - Khmer")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_KHMER"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_KHMER"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_KHMER"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end


-- 'Nam dummy policy

local iCiv = GameInfoTypes["CIVILIZATION_VIET"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

print("dummy policy loaded - Vietnam")
function DummyPolicy(player)
	print("working - Vietnam")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_VIET"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_VIETNAM"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_VIETNAM"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end

-- Norway dummy policy

local iCiv = GameInfoTypes["CIVILIZATION_NORSK"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

print("dummy policy loaded - Norway")
function DummyPolicy(player)
	print("working - Norway dummy policy")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_NORSK"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_NORWAY"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_NORWAY"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end

-- Mexico dummy policy

local iCiv = GameInfoTypes["CIVILIZATION_LEXICO"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

print("dummy policy loaded - Mexico")
function DummyPolicy(player)
	print("working - Mexico")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_LEXICO"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_LEXICO"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_LEXICO"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end
-- Maori dummy policy

local iCiv = GameInfoTypes["CIVILIZATION_MC_MAORI"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

print("dummy policy loaded - Maori")
function DummyPolicy(player)
	print("working - Maori")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_MAORI"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_MAORI"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_MAORI"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end
--[[
-- New zealand dummy policy
local iCiv = GameInfoTypes["CIVILIZATION_MC_NEW_ZEALAND"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
function DummyPolicy(player)
	print("working - NZ")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_NEW_ZEALAND"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_NZ"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_NZ"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end --]]
-- Kilwa Dummy policy
local iCiv = GameInfoTypes["CIVILIZATION_MC_KILWA"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
function DummyPolicy(player)
	print("working - Kilwa")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_KILWA"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_KILWA"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_KILWA"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end
-- Turkey dummy Policy
local iCiv = GameInfoTypes["CIVILIZATION_UC_TURKEY"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
function DummyPolicy(player)
	print("working - Turkey")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_UC_TURKEY"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_TURKEY"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_TURKEY"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end
-- Ireland dummy Policy
local iCiv = GameInfoTypes["CIVILIZATION_IRELAND"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
function DummyPolicy(player)
	print("working - Ireland")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_IRELAND"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_IRELAND"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_IRELAND"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end
-- Scotland dummy Policy
local iCiv = GameInfoTypes["CIVILIZATION_MC_SCOTLAND"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
function DummyPolicy(player)
	print("working - Wales")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_SCOTLAND"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_SCOTLAND"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_SCOTLAND"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end
-- Romania dummy Policy
local iCiv = GameInfoTypes["CIVILIZATION_MC_ROMANIA"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
function DummyPolicy(player)
	print("working - Romania")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_MC_ROMANIA"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_ROMANIA"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_ROMANIA"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end
-- Goth dummy Policy
local iCiv = GameInfoTypes["CIVILIZATION_GOTH"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
function DummyPolicy(player)
	print("working - Goth")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_GOTH"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_GOTH"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_GOTH"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end
-- German dummy policy
local iCiv = GameInfoTypes["CIVILIZATION_GERMANY"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
function DummyPolicy(player)
	print("working - Germany")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_GERMANY"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_GERMANY"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_GERMANY"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end
-- Korea dummy policy

local iCiv = GameInfoTypes["CIVILIZATION_KOREA"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
function DummyPolicy(player)
	print("working - Korea")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_KOREA"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_KOREA"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_KOREA"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end
-- Akkad dummy policy
local iCiv = GameInfoTypes["CIVILIZATION_LITE_AKKAD"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
function DummyPolicy(player)
	print("working - Akkad")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_LITE_AKKAD"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_AKKAD"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_AKKAD"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end
-- Prussia dummy policy
local iCiv = GameInfoTypes["CIVILIZATION_PRUSSIA"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
function DummyPolicy(player)
	print("working - Prussia")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_PRUSSIA"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_PRUSSIA"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_PRUSSIA"], true)	
			end
		end
	end 
end
if bIsActive then
Events.SequenceGameInitComplete.Add(DummyPolicy)
end

-- Akkad_Laputtu
-- Author: Tomatekh
-- DateCreated: 6/22/2015 6:11:46 PM
--------------------------------------------------------------

local Laputtu = GameInfoTypes.UNIT_LITE_AKKAD_LAPUTTU;
local Pyramids = GameInfoTypes.BUILDINGCLASS_PYRAMID;

function AkkadOverseer(iPlayer)
	local pPlayer = Players[iPlayer];
	local pTeam = pPlayer:GetTeam();
	if (pPlayer:IsAlive()) then
		for pUnit in pPlayer:Units() do
			if (pUnit:GetUnitType() == Laputtu) then
				if not pUnit:IsEmbarked() then 
					local uPlot = pUnit:GetPlot();
					local WorkBonus = 100;
					if pPlayer:GetBuildingClassCount(Pyramids) >= 1 then
						WorkBonus = 125;
					end
					for pBuildInfo in GameInfo.Builds() do
						local BuildTurns = uPlot:GetBuildTurnsLeft(pBuildInfo.ID, 0, 0);
						local BuildProgress = uPlot:GetBuildProgress(pBuildInfo.ID)
						if (BuildTurns >= 1) and (BuildProgress > WorkBonus) then
							uPlot:ChangeBuildProgress(pBuildInfo.ID, WorkBonus, pTeam)
						end
					end
				end
			end
		end
	end
end
GameEvents.PlayerDoTurn.Add(AkkadOverseer)


-- AKKAD UA --

include("PlotIterators")
--_________________________________________________________________________________________________________________________________________________________________________________________________________
local iCiv = GameInfoTypes["CIVILIZATION_LITE_AKKAD"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
--_________________________________________________________________________________________________________________________________________________________________________________________________________
local civilizationID = GameInfoTypes["CIVILIZATION_LITE_AKKAD"]
local promotionID = GameInfoTypes["PROMOTION_LITE_AKKAD_CITY_BONUS"]
local greatGeneralPoints = 2
--_________________________________________________________________________________________________________________________________________________________________________________________________________
--GREAT GENERAL BONUS AGAINST CITIES
--_________________________________________________________________________________________________________________________________________________________________________________________________________
function liteGreatGeneralBonusReset(playerID)
	local player = Players[playerID]
	if player:GetCivilizationType() == civilizationID then
		local greatGenerals = {}
		for unit in player:Units() do
			if unit:IsHasPromotion(promotionID) then
				unit:SetHasPromotion(promotionID, false)
			end
			if unit:IsHasPromotion(GameInfoTypes["PROMOTION_GREAT_GENERAL"]) then
				table.insert(greatGenerals, unit)
			end
		end
		for key,greatGeneral in pairs(greatGenerals) do 
			local plot = greatGeneral:GetPlot()
			for loopPlot in PlotAreaSweepIterator(plot, 2, SECTOR_NORTH, DIRECTION_CLOCKWISE, DIRECTION_OUTWARDS, CENTRE_INCLUDE) do
				for i = 0, loopPlot:GetNumUnits() - 1, 1 do  
					local otherUnit = loopPlot:GetUnit(i)
					if otherUnit and otherUnit:GetOwner() == playerID and otherUnit:IsCombatUnit() and not(otherUnit:GetDomainType() == DomainTypes.DOMAIN_SEA) then
						otherUnit:SetHasPromotion(promotionID, true)
					end
				end
			end
		end
	end
end

function liteGreatGeneralBonusAgainstCities(playerID, unitID, unitX, unitY)
	local player = Players[playerID]
	if player:GetCivilizationType() == civilizationID and (player:GetUnitClassCount(GameInfoTypes["UNITCLASS_GREAT_GENERAL"]) > 0) then
		local unit = player:GetUnitByID(unitID)
		local plot = unit:GetPlot()
		if unit:IsHasPromotion(GameInfoTypes["PROMOTION_GREAT_GENERAL"]) then
			for loopPlot in PlotAreaSweepIterator(plot, 2, SECTOR_NORTH, DIRECTION_CLOCKWISE, DIRECTION_OUTWARDS, CENTRE_INCLUDE) do
				for i = 0, loopPlot:GetNumUnits() - 1, 1 do  
					local otherUnit = loopPlot:GetUnit(i)
					if otherUnit and otherUnit:GetOwner() == playerID and otherUnit:IsCombatUnit() and not(otherUnit:GetDomainType() == DomainTypes.DOMAIN_SEA) then
						otherUnit:SetHasPromotion(promotionID, true)
					end
				end
			end
		elseif unit:IsCombatUnit() and not(unit:GetDomainType() == DomainTypes.DOMAIN_SEA) then
			unit:SetHasPromotion(promotionID, false)
			for loopPlot in PlotAreaSweepIterator(plot, 2, SECTOR_NORTH, DIRECTION_CLOCKWISE, DIRECTION_OUTWARDS, CENTRE_INCLUDE) do
				for i = 0, loopPlot:GetNumUnits() - 1, 1 do  
					local otherUnit = loopPlot:GetUnit(i)
					if otherUnit and otherUnit:GetOwner() == playerID and otherUnit:IsHasPromotion(GameInfoTypes["PROMOTION_GREAT_GENERAL"]) then
						unit:SetHasPromotion(promotionID, true)
					end
				end
			end
		end
	end
end


--_________________________________________________________________________________________________________________________________________________________________________________________________________
--GREAT GENERAL POINTS FROM IMPROVEMENTS
--_________________________________________________________________________________________________________________________________________________________________________________________________________
function liteGreatGeneralPointsfromImproving(playerID, plotX, plotY, improvementID) 
	local player = Players[playerID]
	if improvementID then
		if player:GetCivilizationType() == civilizationID then
			local plot = Map.GetPlot(plotX, plotY)
			local city = plot:GetWorkingCity()
			if city and city:IsOccupied() then
				player:ChangeCombatExperience(greatGeneralPoints)
			end
		end
	end
end

if bIsActive then
GameEvents.PlayerDoTurn.Add(liteGreatGeneralBonusReset)
GameEvents.UnitSetXY.Add(liteGreatGeneralBonusAgainstCities)
GameEvents.BuildFinished.Add(liteGreatGeneralPointsfromImproving)
end
--_________________________________________________________________________________________________________________________________________________________________________________________________________


local iCiv = GameInfoTypes["CIVILIZATION_AZTEC"]
local bIsActive = JFD_IsCivilisationActive(iCiv)


function AztecBorderExpandOnTrain(playerID, cityID)
	print('City Trained')
	local player = Players[playerID]
	local city = player:GetCityByID(cityID)
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_AZTEC"] and city then
			-- Add culture to the city to cause a border expansion
			local cultureNeeded = city:GetJONSCultureThreshold() - city:GetJONSCultureStored()
			if cultureNeeded > 0 then
					city:ChangeJONSCultureStored(cultureNeeded)
			end
	end
end
if bIsActive then
	GameEvents.CityTrained.Add(AztecBorderExpandOnTrain)
end


-- ProphetReplacer
-- Author: LastSword
-- DateCreated: 8/24/2013 2:56:18 PM
--------------------------------------------------------------
local sUnitType = "UNIT_PROPHET"
local iProphetID = GameInfo.Units.UNIT_PROPHET.ID
local iProphetOverride = GameInfo.Units.UNIT_DALAILAMA.ID
local iCivType = GameInfo.Civilizations["CIVILIZATION_TIBET"].ID
local iCiv = GameInfoTypes["CIVILIZATION_TIBET"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

function TibetOverride(iPlayer, iUnit)
    local pPlayer = Players[iPlayer];
    if (pPlayer:IsEverAlive()) then
        if (pPlayer:GetCivilizationType() == iCivType) then
      	    if pPlayer:GetUnitByID(iUnit) ~= nil then
		pUnit = pPlayer:GetUnitByID(iUnit);
               if (pUnit:GetUnitType() == iProphetID) then
                   local newUnit = pPlayer:InitUnit(iProphetOverride, pUnit:GetX(), pUnit:GetY())
                    newUnit:Convert(pUnit);
                end
            end
        end
    end
end

if bIsActive then
	Events.SerialEventUnitCreated.Add(TibetOverride)
end

-- mpiambina
-- Author: lek10
-- DateCreated: 11/21/2018 5:29:36 PM
--------------------------------------------------------------
local sUnitType = "UNIT_INQUISITOR"
local iProphetID = GameInfo.Units.UNIT_INQUISITOR.ID
local iProphetOverride = GameInfo.Units.UNIT_MPIAMBINA.ID
local iCivType = GameInfo.Civilizations["CIVILIZATION_MALAGASY"].ID
local iCiv = GameInfoTypes["CIVILIZATION_MALAGASY"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

function MadaOverride(iPlayer, iUnit)
    local pPlayer = Players[iPlayer];
    if (pPlayer:IsEverAlive()) then
        if (pPlayer:GetCivilizationType() == iCivType) then
       	    if pPlayer:GetUnitByID(iUnit) ~= nil then
		pUnit = pPlayer:GetUnitByID(iUnit);
                if (pUnit:GetUnitType() == iProphetID) then
                    local newUnit = pPlayer:InitUnit(iProphetOverride, pUnit:GetX(), pUnit:GetY())
                    newUnit:Convert(pUnit);
                end
            end
        end
    end
end

if bIsActive then
	Events.SerialEventUnitCreated.Add(MadaOverride)
end


----////--------------------------------------------------
				--- ARGENTINA WORKER ---
----////--------------------------------------------------

local OldUnitType = "UNIT_WORKER"
local oldUnitID = GameInfo.Units.UNIT_WORKER.ID
local newUnitID = GameInfo.Units.UNIT_ARGENTINA_WORKER.ID
local iCivType = GameInfo.Civilizations["CIVILIZATION_ARGENTINA"].ID
local iCiv = GameInfoTypes["CIVILIZATION_ARGENTINA"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

function ArgentinaWorkerOverride(iPlayer, iUnit)
    local pPlayer = Players[iPlayer];
    if (pPlayer:IsEverAlive()) then
        if (pPlayer:GetCivilizationType() == iCivType) then
       	    if pPlayer:GetUnitByID(iUnit) ~= nil then
		pUnit = pPlayer:GetUnitByID(iUnit);
                if (pUnit:GetUnitType() == oldUnitID) then
                    local newUnit = pPlayer:InitUnit(newUnitID, pUnit:GetX(), pUnit:GetY())
                    newUnit:Convert(pUnit);
                end
            end
        end
    end
end

if bIsActive then
	Events.SerialEventUnitCreated.Add(ArgentinaWorkerOverride)
end


-- Now reverse!! I typed this backwards so you will get a better explantion below in similar functions but right now I couldnt be bothered copy-pasting so I decided instead, to write this novel. ~EAP

local OldUnitType = "UNIT_ARGENTINA_WORKER"
local oldUnitID = GameInfo.Units.UNIT_ARGENTINA_WORKER.ID
local newUnitID = GameInfo.Units.UNIT_WORKER.ID
local iCivType = GameInfo.Civilizations["CIVILIZATION_ARGENTINA"].ID

function ArgentinaWorkerROverride(iPlayer, iUnit)
    local pPlayer = Players[iPlayer];
    if (pPlayer:IsEverAlive()) then
        if (not pPlayer:GetCivilizationType() == iCivType) then
       	    if pPlayer:GetUnitByID(iUnit) ~= nil then
		pUnit = pPlayer:GetUnitByID(iUnit);
                if (pUnit:GetUnitType() == oldUnitID) then
                    local newUnit = pPlayer:InitUnit(newUnitID, pUnit:GetX(), pUnit:GetY())
                    newUnit:Convert(pUnit);
                end
            end
        end
    end
end

if bIsActive then
	Events.SerialEventUnitCreated.Add(ArgentinaWorkerROverride)
end

----////--------------------------------------------------
				--- CHILE ADMIRAL ---
----////--------------------------------------------------

local OldUnitType = "UNIT_GREAT_ADMIRAL"
local oldUnitID = GameInfo.Units.UNIT_GREAT_ADMIRAL.ID
local newUnitID = GameInfo.Units.UNIT_CHILE_GREAT_ADMIRAL.ID
local iCivType = GameInfo.Civilizations["CIVILIZATION_CHILE"].ID
local iCiv = GameInfoTypes["CIVILIZATION_CHILE"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

function ChileAdmiralOverride(iPlayer, iUnit)
    local pPlayer = Players[iPlayer];
    if (pPlayer:IsEverAlive()) then
        if (pPlayer:GetCivilizationType() == iCivType) then
       	    if pPlayer:GetUnitByID(iUnit) ~= nil then
		pUnit = pPlayer:GetUnitByID(iUnit);
                if (pUnit:GetUnitType() == oldUnitID) then
                    local newUnit = pPlayer:InitUnit(newUnitID, pUnit:GetX(), pUnit:GetY())
                    newUnit:Convert(pUnit);
                end
            end
        end
    end
end

if bIsActive then
	Events.SerialEventUnitCreated.Add(ChileAdmiralOverride)
end

--- Now reverse it so other civs wont get it by various means
local OldUnitType = "UNIT_CHILE_GREAT_ADMIRAL"
local oldUnitID = GameInfo.Units.UNIT_CHILE_GREAT_ADMIRAL.ID
local newUnitID = GameInfo.Units.UNIT_GREAT_ADMIRAL.ID
local iCivType = GameInfo.Civilizations["CIVILIZATION_CHILE"].ID

function ChileAdmiralROverride(iPlayer, iUnit)
    local pPlayer = Players[iPlayer];
    if (pPlayer:IsEverAlive()) then
        if (not pPlayer:GetCivilizationType() == iCivType) then
       	    if pPlayer:GetUnitByID(iUnit) ~= nil then
		pUnit = pPlayer:GetUnitByID(iUnit);
                if (pUnit:GetUnitType() == oldUnitID) then
                    local newUnit = pPlayer:InitUnit(newUnitID, pUnit:GetX(), pUnit:GetY())
                    newUnit:Convert(pUnit);
                end
            end
        end
    end
end

if bIsActive then
	Events.SerialEventUnitCreated.Add(ChileAdmiralROverride)
end

----////--------------------------------------------------
				--- VIETNAM WORKER ---
----////--------------------------------------------------

local OldUnitType = "UNIT_WORKER"
local oldUnitID = GameInfo.Units.UNIT_WORKER.ID
local newUnitID = GameInfo.Units.UNIT_VIETNAM_WORKER.ID
local iCivType = GameInfo.Civilizations["CIVILIZATION_VIET"].ID
local iCiv = GameInfoTypes["CIVILIZATION_VIET"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

function VietnamWorkerOverride(iPlayer, iUnit)
    local pPlayer = Players[iPlayer];
    if (pPlayer:IsEverAlive()) then
        if (pPlayer:GetCivilizationType() == iCivType) then
       	    if pPlayer:GetUnitByID(iUnit) ~= nil then
		pUnit = pPlayer:GetUnitByID(iUnit);
                if (pUnit:GetUnitType() == oldUnitID) then
                    local newUnit = pPlayer:InitUnit(newUnitID, pUnit:GetX(), pUnit:GetY())
                    newUnit:Convert(pUnit);
                end
            end
        end
    end
end

if bIsActive then
	Events.SerialEventUnitCreated.Add(VietnamWorkerOverride)
end


-- Now reverse!! I typed this backwards so you will get a better explantion below in similar functions but right now I couldnt be bothered copy-pasting so I decided instead, to write this novel. ~EAP

local OldUnitType = "UNIT_VIETNAM_WORKER"
local oldUnitID = GameInfo.Units.UNIT_VIETNAM_WORKER.ID
local newUnitID = GameInfo.Units.UNIT_WORKER.ID
local iCivType = GameInfo.Civilizations["CIVILIZATION_VIET"].ID

function ArgentinaWorkerROverride(iPlayer, iUnit)
    local pPlayer = Players[iPlayer];
    if (pPlayer:IsEverAlive()) then
        if (not pPlayer:GetCivilizationType() == iCivType) then
       	    if pPlayer:GetUnitByID(iUnit) ~= nil then
		pUnit = pPlayer:GetUnitByID(iUnit);
                if (pUnit:GetUnitType() == oldUnitID) then
                    local newUnit = pPlayer:InitUnit(newUnitID, pUnit:GetX(), pUnit:GetY())
                    newUnit:Convert(pUnit);
                end
            end
        end
    end
end

if bIsActive then
	Events.SerialEventUnitCreated.Add(VietnamWorkerROverride)
end



---------------------
-- On Policy Adopted stuff (uncludes UA's , easier for me to copy pasta stuff <3)
---------------

-- Notes, do realize that this is coded keeping policy requirements/paths in mind, so if you change any of the policy paths in the future, if anyone besides me is doing policies stuff, DONT FORGET TO CHECK THIS! thnx! ~EAP

-- Italy ua

local iCiv = GameInfoTypes["CIVILIZATION_ITALY"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
function Italy_OnPolicyAdopted(playerID, policyID)

	local player = Players[playerID]

	-- Piety finished
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_ITALY"] then

		if	(policyID == GameInfo.Policies["POLICY_THEOCRACY"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_MANDATE_OF_HEAVEN"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_FREE_RELIGION"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_REFORMATION"].ID)) or
			(policyID == GameInfo.Policies["POLICY_MANDATE_OF_HEAVEN"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_THEOCRACY"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_FREE_RELIGION"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_REFORMATION"].ID)) or
			(policyID == GameInfo.Policies["POLICY_FREE_RELIGION"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_MANDATE_OF_HEAVEN"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_THEOCRACY"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_REFORMATION"].ID)) or
			(policyID == GameInfo.Policies["POLICY_REFORMATION"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_MANDATE_OF_HEAVEN"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_FREE_RELIGION"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_THEOCRACY"].ID)) then

			-- Finished Policy Tree, now add the building
			local pCity = player:GetCapitalCity();
			pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_ITALY_TRAIT_PIETY"], 1);
		end
	end 
end

if bIsActive then
GameEvents.PlayerAdoptPolicy.Add(Italy_OnPolicyAdopted);
end
function Italy_OnPolicyAdopted(playerID, policyID)

	local player = Players[playerID]

	-- Tradition Finished
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_ITALY"] then

		if	(policyID == GameInfo.Policies["POLICY_ARISTOCRACY"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_LANDED_ELITE"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_MONARCHY"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_OLIGARCHY"].ID)) or
			(policyID == GameInfo.Policies["POLICY_LANDED_ELITE"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_ARISTOCRACY"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_MONARCHY"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_OLIGARCHY"].ID)) or
			(policyID == GameInfo.Policies["POLICY_MONARCHY"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_LANDED_ELITE"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_ARISTOCRACY"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_OLIGARCHY"].ID)) or
			(policyID == GameInfo.Policies["POLICY_OLIGARCHY"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_LANDED_ELITE"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_MONARCHY"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_ARISTOCRACY"].ID)) then

			-- Finished Policy Tree, now add the building
			local pCity = player:GetCapitalCity();
			pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_ITALY_TRAIT_TRADITION"], 1);
		end
	end 
end

if bIsActive then
GameEvents.PlayerAdoptPolicy.Add(Italy_OnPolicyAdopted);
end
function Italy_OnPolicyAdopted(playerID, policyID)

	local player = Players[playerID]

	-- Liberty Finished
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_ITALY"] then

		if	(policyID == GameInfo.Policies["POLICY_COLLECTIVE_RULE"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_REPRESENTATION"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_MERITOCRACY"].ID)) or
			(policyID == GameInfo.Policies["POLICY_REPRESENTATION"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_COLLECTIVE_RULE"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_MERITOCRACY"].ID)) or
			(policyID == GameInfo.Policies["POLICY_MERITOCRACY"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_COLLECTIVE_RULE"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_REPRESENTATION"].ID)) then

			-- Finished Policy Tree, now add the building
			local pCity = player:GetCapitalCity();
			pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_ITALY_TRAIT_LIBERTY"], 1);
		end
	end 
end
if bIsActive then
GameEvents.PlayerAdoptPolicy.Add(Italy_OnPolicyAdopted);
end

function Italy_OnPolicyAdopted(playerID, policyID)

	local player = Players[playerID]

	-- Honor Finished
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_ITALY"] then

		if	(policyID == GameInfo.Policies["POLICY_MILITARY_CASTE"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_MILITARY_TRADITION"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_DISCIPLINE"].ID)) or
			(policyID == GameInfo.Policies["POLICY_MILITARY_TRADITION"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_MILITARY_CASTE"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_DISCIPLINE"].ID)) or
			(policyID == GameInfo.Policies["POLICY_DISCIPLINE"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_MILITARY_CASTE"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_MILITARY_TRADITION"].ID)) then

			-- Finished Policy Tree, now add the building
			local pCity = player:GetCapitalCity();
			pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_ITALY_TRAIT_HONOR"], 1);
		end
	end 
end
if bIsActive then
GameEvents.PlayerAdoptPolicy.Add(Italy_OnPolicyAdopted);
end

function Italy_OnPolicyAdopted(playerID, policyID)

	local player = Players[playerID]

	-- Patronage Finished
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_ITALY"] then

		if	(policyID == GameInfo.Policies["POLICY_CONSULATES"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_SCHOLASTICISM"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_PHILANTHROPY"].ID)) or
			(policyID == GameInfo.Policies["POLICY_SCHOLASTICISM"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_CONSULATES"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_PHILANTHROPY"].ID)) or
			(policyID == GameInfo.Policies["POLICY_PHILANTHROPY"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_CONSULATES"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_SCHOLASTICISM"].ID)) then

			-- Finished Policy Tree, now add the building
			local pCity = player:GetCapitalCity();
			pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_ITALY_TRAIT_PATRONAGE"], 1);
		end
	end 
end
if bIsActive then
GameEvents.PlayerAdoptPolicy.Add(Italy_OnPolicyAdopted);
end

function Italy_OnPolicyAdopted(playerID, policyID)

	local player = Players[playerID]

	-- Aesthetics Finished
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_ITALY"] then

		if	(policyID == GameInfo.Policies["POLICY_FINE_ARTS"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_FLOURISHING_OF_ARTS"].ID)) or
			(policyID == GameInfo.Policies["POLICY_FLOURISHING_OF_ARTS"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_FINE_ARTS"].ID)) then

			-- Finished Policy Tree, now add the building
			local pCity = player:GetCapitalCity();
			pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_ITALY_TRAIT_AESTHETICS"], 1);
		end
	end 
end

if bIsActive then
GameEvents.PlayerAdoptPolicy.Add(Italy_OnPolicyAdopted);
end
function Italy_OnPolicyAdopted(playerID, policyID)

	local player = Players[playerID]

	-- Exploration Finished
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_ITALY"] then

		if	(policyID == GameInfo.Policies["POLICY_TREASURE_FLEETS"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_MERCHANT_NAVY"].ID)) or
			(policyID == GameInfo.Policies["POLICY_MERCHANT_NAVY"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_TREASURE_FLEETS"].ID)) then

			-- Finished Policy Tree, now add the building
			local pCity = player:GetCapitalCity();
			pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_ITALY_TRAIT_EXPLORATION"], 1);
		end
	end 
end
if bIsActive then
GameEvents.PlayerAdoptPolicy.Add(Italy_OnPolicyAdopted);
end

function Italy_OnPolicyAdopted(playerID, policyID)

	local player = Players[playerID]

	-- Rationalism Finished
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_ITALY"] then

		if	(policyID == GameInfo.Policies["POLICY_SECULARISM"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_HUMANISM"].ID)) or
			(policyID == GameInfo.Policies["POLICY_HUMANISM"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_SECULARISM"].ID)) then

			-- Finished Policy Tree, now add the building
			local pCity = player:GetCapitalCity();
			pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_ITALY_TRAIT_RATIONALISM"], 1);
		end
	end 
end
if bIsActive then
GameEvents.PlayerAdoptPolicy.Add(Italy_OnPolicyAdopted);
end


function Italy_OnPolicyAdopted(playerID, policyID)

	local player = Players[playerID]

	-- Commerce Finished
	if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_ITALY"] then

		if	(policyID == GameInfo.Policies["POLICY_ENTREPRENEURSHIP"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_PROTECTIONISM"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_TRADE_UNIONS"].ID)) or
			(policyID == GameInfo.Policies["POLICY_PROTECTIONISM"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_ENTREPRENEURSHIP"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_TRADE_UNIONS"].ID)) or
			(policyID == GameInfo.Policies["POLICY_TRADE_UNIONS"].ID 
			and player:HasPolicy(GameInfo.Policies["POLICY_ENTREPRENEURSHIP"].ID)
			and player:HasPolicy(GameInfo.Policies["POLICY_PROTECTIONISM"].ID)) then

			-- Finished Policy Tree, now add the building
			local pCity = player:GetCapitalCity();
			pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_ITALY_TRAIT_COMMERCE"], 1);
		end
	end 
end

if bIsActive then
GameEvents.PlayerAdoptPolicy.Add(Italy_OnPolicyAdopted);
end

-- Arsenal of Democracy Updates
-- Author: Glossen
--DateCreated: 3/19/2023 11:38:ish PM
--------------------------------------------------------------
function Arsenal_Democracy_OnPolicyAdopted(playerID, policyID)

	local player = Players[playerID]
	local hasArsenal_Democracy = (policyID==GameInfo.Policies["POLICY_ARSENAL_DEMOCRACY"].ID)

	if	(hasArsenal_Democracy) then

		for pCity in player:Cities() do
			pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_ARSENAL_DEMOCRACY"], 1)
		end
	end
end
GameEvents.PlayerAdoptPolicy.Add(Arsenal_Democracy_OnPolicyAdopted);

-- Alternative Energy Uranium spawn
-- Author: Ashwin 
-- DateCreated: 9/5/2023
-------------------------------------------------------------
function Alternative_Energy_OnPolicyAdopted(playerID, policyID)

	local player = Players[playerID]

	if (policyID == GameInfo.Policies["POLICY_ALTERNATIVE_ENERGY"].ID) then 
		local UraniumBuilding = GameInfoTypes["BUILDING_URANIUM_REFINERY"];
		local pCity = player:GetCapitalCity(); 
		local iNumToSet = pCity:GetNumBuilding(UraniumBuilding) + 1
		pCity:SetNumRealBuilding(UraniumBuilding, iNumToSet)
	end
end

GameEvents.PlayerAdoptPolicy.Add(Alternative_Energy_OnPolicyAdopted);

-- Economic union free trade route
-- Author: Ashwin
-- DateCreated: 9/5/2023
-------------------------------------------------------------

function EU_OnPolicyAdopted(playerID, policyID)

	local player = Players[playerID]

	if (policyID == GameInfo.Policies["POLICY_ECONOMIC_UNION"].ID) then 
		local pCity = player:GetCapitalCity(); 
		pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_EU_TRADE"], 1); 
	end
end

GameEvents.PlayerAdoptPolicy.Add(EU_OnPolicyAdopted);

-- Economic union yields
-- Author: Ashwin
-- DateCreated: 9/5/2023

function Economic_Union(playerID)

	local player = Players[playerID]
	local building_Food = GameInfoTypes["BUILDING_EU_FOOD"]
	local building_Prod = GameInfoTypes["BUILDING_EU_PROD"]

	if player:HasPolicy(GameInfo.Policies["POLICY_ECONOMIC_UNION"].ID) then
		local tradeRoutes = player:GetTradeRoutes();
		local numRoutesInternal = 0; local numRoutesExternal = 0;
		for city in player:Cities() do
			numRoutesInternal = 0;
			numRoutesExternal = 0;
			for tradeRouteID, tradeRoute in ipairs(tradeRoutes) do
				if tradeRoute.FromCity == city then
					if tradeRoute.ToID == playerID then
						numRoutesInternal = numRoutesInternal + 1;
					else
						numRoutesExternal = numRoutesExternal + 1;
					end
				end
			end
			city:SetNumRealBuilding(building_Food, numRoutesExternal);
			city:SetNumRealBuilding(building_Prod, numRoutesInternal);
		end
	end
end 

GameEvents.PlayerDoTurn.Add(Economic_Union);

-- PietyChanges
-- Author: Cirra
-- DateCreated: 10/17/2019 1:22:18 AM
--------------------------------------------------------------
function Patronage_OnPolicyAdopted(playerID, policyID)

	local player = Players[playerID]
	
	if	(policyID == GameInfo.Policies["POLICY_CONSULATES"].ID) then
		local pCity = player:GetCapitalCity();
		pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_PIETY_FINISHER"], 1);
	end
end

GameEvents.PlayerAdoptPolicy.Add(Patronage_OnPolicyAdopted);

-- HonorChanges
-- Author: Cirra
-- DateCreated: 7/27/2019 1:22:18 AM
--------------------------------------------------------------

function Honor_OnPolicyAdopted(playerID, policyID)

	local player = Players[playerID]

	-- Honor Finisher
	if	(policyID == GameInfo.Policies["POLICY_DISCIPLINE"].ID 
		and player:HasPolicy(GameInfo.Policies["POLICY_MILITARY_TRADITION"].ID)
		and player:HasPolicy(GameInfo.Policies["POLICY_MILITARY_CASTE"].ID)) or
		(policyID == GameInfo.Policies["POLICY_MILITARY_TRADITION"].ID 
		and player:HasPolicy(GameInfo.Policies["POLICY_DISCIPLINE"].ID)
		and player:HasPolicy(GameInfo.Policies["POLICY_MILITARY_CASTE"].ID)) or
		(policyID == GameInfo.Policies["POLICY_MILITARY_CASTE"].ID 
		and player:HasPolicy(GameInfo.Policies["POLICY_MILITARY_TRADITION"].ID)
		and player:HasPolicy(GameInfo.Policies["POLICY_DISCIPLINE"].ID)) then

		-- The player has finished Honor. Add old ToA to the capital, which gives +10% food everywhere.
		local pCity = player:GetCapitalCity();
		pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_HONOR_FINISHER"], 1);

	end
end
GameEvents.PlayerAdoptPolicy.Add(Honor_OnPolicyAdopted);

local HONOR_HAPPY_BUILDING = GameInfoTypes["BUILDING_HONOR_GLOBAL_HAPPY"]

function Honor_CheckBuiltBuildingIsNationalWonder(playerID, cityID, buildingID)
	local player = Players[playerID]
	if (player:HasPolicy(GameInfo.Policies["POLICY_MILITARY_CASTE"].ID)) then
		if (isNationalWonder(buildingID)) then
			print('National Wonder constructed, applying honor happiness')
			local city = player:GetCityByID(cityID)
			local iNumToSet = city:GetNumBuilding(HONOR_HAPPY_BUILDING) + 1
			city:SetNumRealBuilding(HONOR_HAPPY_BUILDING, iNumToSet)
		end
	end
end
GameEvents.CityConstructed.Add(Honor_CheckBuiltBuildingIsNationalWonder);

function Honor_AddGlobalHappinessFromWondersOnPolicy(playerID, policyId)
	local player = Players[playerID]
	if (player:IsEverAlive() and policyId == GameInfo.Policies["POLICY_MILITARY_CASTE"].ID) then
		for city in player:Cities() do
			local numNationalWonders = countNationalWondersInCity(city)
      city:SetNumRealBuilding(HONOR_HAPPY_BUILDING, numNationalWonders)
		end
	end
end
GameEvents.PlayerAdoptPolicy.Add(Honor_AddGlobalHappinessFromWondersOnPolicy);

local TRAD_HAPPY_BUILDING = GameInfoTypes["BUILDING_TRAD_GLOBAL_HAPPY"]

function Trad_CheckBuiltBuildingIsNationalWonder(playerID, cityID, buildingID)
	local player = Players[playerID]
	if (player:HasPolicy(GameInfo.Policies["POLICY_TRADITION_FINISHER"].ID)) then
		if (isNationalWonder(buildingID)) then
			print('National Wonder constructed, applying trad happiness')
			local city = player:GetCityByID(cityID)
			local iNumToSet = city:GetNumBuilding(TRAD_HAPPY_BUILDING) + 1
			city:SetNumRealBuilding(TRAD_HAPPY_BUILDING, iNumToSet)
		end
	end
end
GameEvents.CityConstructed.Add(Trad_CheckBuiltBuildingIsNationalWonder);

function Trad_AddGlobalHappinessFromWondersOnPolicy(playerID, policyID)
    local player = Players[playerID]
    local policy = GameInfo.Policies[policyID]
    local policy_branch = GameInfo.PolicyBranchTypes[policy.PolicyBranchType]

    if player:IsEverAlive() then
        if policy_branch == GameInfoTypes["POLICY_BRANCH_TRADITION"] and player:IsPolicyBranchFinished(policy_branch.ID) then
			if Game.GetActivePlayer() == playerID then
				for city in player:Cities() do
					local numNationalWonders = 0
					for building in GameInfo.Buildings() do
						if (city:IsHasBuilding(building.ID) and isNationalWonder(building.ID)) then
							numNationalWonders = numNationalWonders + 1
						end
					end
					city:SetNumRealBuilding(TRAD_HAPPY_BUILDING, numNationalWonders)
				end
			end
		end
	end
end
GameEvents.PlayerAdoptPolicy.Add(Trad_AddGlobalHappinessFromWondersOnPolicy);


-- piety reformation food changes
-- author: Ashwin

function Piety_AddHolyCityFoodOnPolicy(playerID, policyID)
	local player = Players[playerID]
	local foodBuilding = GameInfoTypes["BUILDING_DUMMY_FOOD"]
	if (player:IsEverAlive() and policyID == GameInfo.Policies["POLICY_REFORMATION"].ID) then
		for city in player:Cities() do
			if city:IsHolyCityAnyReligion() then
				local iNumToSet = city:GetNumBuilding(foodBuilding) + 2
				city:SetNumRealBuilding(foodBuilding, iNumToSet)
			end
		end
	end
end
GameEvents.PlayerAdoptPolicy.Add(Piety_AddHolyCityFoodOnPolicy)

function Piety_AddHolyCityFoodOnFound(playerID, holyCityID, belief1, belief2, belief3, belief4)
	local player = Players[playerID]
	local foodBuilding = GameInfoTypes["BUILDING_DUMMY_FOOD"]
	if (player:IsEverAlive() and player:HasPolicy(GameInfoTypes["POLICY_REFORMATION"])) then
		city = player:GetCityByID(holyCityID)
		local iNumToSet = city:GetNumBuilding(foodBuilding) + 2
		city:SetNumRealBuilding(foodBuilding, iNumToSet)
	end
end
GameEvents.ReligionFounded.Add(Piety_AddHolyCityFoodOnFound)


function Patronage_MC_OnPolicyAdopted(playerID, policyID)
	
	local player = Players[playerID];
	if (player:IsEverAlive() and policyID == GameInfo.Policies["POLICY_MERCHANT_CONFEDERACY"].ID ) then
		local pCity = player:GetCapitalCity();
		pCity:SetNumRealBuilding(GameInfoTypes["BUILDING_CAP_EMBASSAY"], 1);
	end
end
GameEvents.PlayerAdoptPolicy.Add(Patronage_MC_OnPolicyAdopted);


-- Mexico dummy policy
print("dummy policy loaded - Mexico")
function DummyPolicy(player)
	print("working - Mexico")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_LEXICO"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_MEXICO"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_MEXICO"], true)	
			end
		end
	end 
end
Events.SequenceGameInitComplete.Add(DummyPolicy)

-- Ethiopia dummy policy
print("dummy policy loaded - Ethiopia")
function DummyPolicy(player)
	print("working - Ethiopia")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_ETHIOPIA"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_ETHIOPIA"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_ETHIOPIA"], true)	
			end
		end
	end 
end
Events.SequenceGameInitComplete.Add(DummyPolicy)

-- Ethiopia dummy policy
print("dummy policy loaded - Ethiopia")
function DummyPolicy(player)
	print("working - Ethiopia")
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_ETHIOPIA"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_ETHIOPIA"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_ETHIOPIA"], true)	
			end
		end
	end 
end
Events.SequenceGameInitComplete.Add(DummyPolicy)

-- England dummy policy
function DummyPolicy(player)
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_ENGLAND"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_ENGLAND"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_ENGLAND"], true)	
			end
		end
	end 
end
Events.SequenceGameInitComplete.Add(DummyPolicy)

-- Austria dummy policy
function DummyPolicy(player)
	for playerID, player in pairs(Players) do
		local player = Players[playerID];
		if player:GetCivilizationType() == GameInfoTypes["CIVILIZATION_AUSTRIA"] then
			if not player:HasPolicy(GameInfoTypes["POLICY_DUMMY_AUSTRIA"]) then
				
				player:SetNumFreePolicies(1)
				player:SetNumFreePolicies(0)
				player:SetHasPolicy(GameInfoTypes["POLICY_DUMMY_AUSTRIA"], true)	
			end
		end
	end 
end
Events.SequenceGameInitComplete.Add(DummyPolicy)

-- Switzerland policies
-- Author: Ashwin (copying from Lekmod 31.9)
-- DateCreated: 9/14/2023


local civSwiss = GameInfoTypes["CIVILIZATION_SWISS"];
local swissIsActive = JFD_IsCivilisationActive(civSwiss);

local skiResort = GameInfoTypes["BUILDING_SWISS_SKI_RESORT"];
local skiResortM = GameInfoTypes["BUILDING_SWISS_SKI_RESORT_MOUNTAIN"];

local swissTrait = GameInfoTypes["BUILDING_SWISS_TRAIT"];

local mountaineer = GameInfoTypes["PROMOTION_SWISS_MOUNTAINEER"];
local mountaineerActive = GameInfoTypes["PROMOTION_SWISS_MOUNTAINEER_ACTIVE"];

local reislaufer = GameInfoTypes["UNIT_SWISS_REISLAUFER"];

function Mountaineer(playerID, unitID, x, y)
	local player = Players[playerID];
	local unit = player:GetUnitByID(unitID);
	
	if unit:IsHasPromotion(mountaineer) then
		local pPlot = Map.GetPlot(unit:GetX(),unit:GetY());
			
		local isNearMountain = false;
			
		for pAdjacentPlot in PlotAreaSweepIterator(pPlot, 1, SECTOR_NORTH, DIRECTION_CLOCKWISE, DIRECTION_OUTWARDS, CENTRE_EXCLUDE) do
			if pAdjacentPlot:IsMountain() then
				isNearMountain = true;
			end
			if isNearMountain then
				break;
			end
		end
		
		if isNearMountain then
			unit:SetHasPromotion(mountaineerActive, true);
		else
			unit:SetHasPromotion(mountaineerActive, false);
		end
	end	
	
	if player:GetCivilizationType() == civSwiss and player:IsAlive() then
		for pUnit in player:Units() do
			if pUnit:GetUnitType() == reislaufer and not pUnit:IsDead() then
				if pUnit:GetLevel() == 0 and pUnit:GetExperience() == 0 then
					pUnit:ChangeLevel(2);
				end
			end
		end
	end
end

function SwissUA(playerID)
	local player = Players[playerID];
	
	if player:GetCivilizationType() == civSwiss and player:IsAlive() then
		for pUnit in player:Units() do
			if pUnit:GetUnitType() == reislaufer and not pUnit:IsDead() then
				if pUnit:GetLevel() == 1 and pUnit:GetExperience() == 0 then
					pUnit:ChangeExperience(30);
				end
			end
		end
	end
end

if swissIsActive then
GameEvents.PlayerDoTurn.Add(SwissUA);
GameEvents.UnitSetXY.Add(Mountaineer);
end

-- Macedonia General Heal
-- Author: Ashwin
-- Date: 12/11/2024

local civMace = GameInfoTypes["CIVILIZATION_MACEDON"]
local maceIsActive = JFD_IsCivilisationActive(civMace)
local promotionID = GameInfoTypes["PROMOTION_GENERAL_HEAL"]
local generalID = GameInfoTypes["UNIT_GREAT_GENERAL"]
local honorGeneralID = GameInfoTypes["UNIT_HONOR_GENERAL"]

function MaceGeneralHealPromotion(playerID, unitID)
	local pPlayer = Players[playerID]
	if pPlayer and pPlayer:IsAlive() and pPlayer:GetCivilizationType() == civMace then
		local unit = pPlayer:GetUnitByID(unitID)
		if unit:GetUnitType() == generalID or unit:GetUnitType() == honorGeneralID then 
			unit:SetHasPromotion(promotionID, true)
		end
	end
end

if maceIsActive then
	Events.SerialEventUnitCreated.Add(MaceGeneralHealPromotion)
end

-- Hoover Dam 
-- Author: Ashwin

local riverHammers = GameInfoTypes["BUILDING_RIVER_HAMMERS"]
local riverGold = GameInfoTypes["BUILDING_RIVER_GOLD"]

local buildingHoover = GameInfoTypes["BUILDING_HYDRO_WONDER"];
hooverCities = {};
function addHoover(playerID, cityID, buildingID)

	local building = GameInfo.Buildings[buildingID]
    if building.Type == "BUILDING_HYDRO_WONDER" then
		local player = Players[playerID]
		local city = player:GetCityByID(cityID)
		local plotX = city:GetX()
		local plotY = city:GetY()
		city:SetNumRealBuilding(riverHammers, 1)
		table.insert(hooverCities, {plotX, plotY})
		print(string.format('added to hoover table at x: %d, y: %d', plotX, plotY))
	end
end

GameEvents.CityConstructed.Add(addHoover)

function fillHooverTable()
	for playerID, player in pairs(Players) do
		for city in player:Cities() do
			if city:GetNumBuilding(buildingHoover) > 0 then
				table.insert(hooverCities, {city:GetX(), city:GetY()})
				print(string.format('added to hoover table at x: %d, y: %d', city:GetX(), city:GetY()))
			end
		end
	end
end

Events.SequenceGameInitComplete.Add(fillHooverTable)


-- hoover dam alternates river hammers and river gold
function HooverHammerSwitch(playerID)
	for i, v in ipairs(hooverCities) do
		local plot = Map.GetPlot(v[1], v[2])
		local city = plot:GetPlotCity()
		local player = Players[playerID]
		if player:GetCityByID(city:GetID()) == city then
			--print("running hoover switch at " plot.getX() " and " plot.getY())
			local countHammerBuilding = city:GetNumBuilding(riverHammers)
			local countGoldBuilding = city:GetNumBuilding(riverGold)
			city:SetNumRealBuilding(riverHammers, countGoldBuilding)
			city:SetNumRealBuilding(riverGold, countHammerBuilding)
		end
	end
end

GameEvents.PlayerDoTurn.Add(HooverHammerSwitch)

function FourYearPlanBuildingConstructed(playerId, cityId)
	local player = Players[playerId]
	if player:HasPolicy(GameInfo.Policies["POLICY_FOUR_YEAR_PLAN"].ID) then
		print('FourYearPlan: Building constructed, adding cost')
		local city = player:GetCityByID(cityId)
		local numBuildings = city:GetNumBuilding(GameInfoTypes["BUILDING_FOUR_YEAR_PLAN_GOLD"])
		city:SetNumRealBuilding(GameInfoTypes["BUILDING_FOUR_YEAR_PLAN_GOLD"], numBuildings + 1)
	end
end

GameEvents.CityConstructed.Add(FourYearPlanBuildingConstructed)