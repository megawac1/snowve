
--=================================================================================================================
--=================================================================================================================
						-- GLOBALS (from JFD)
--=================================================================================================================
--=================================================================================================================

local g_ConvertTextKey  		= Locale.ConvertTextKey
local g_MapGetPlot				= Map.GetPlot
local g_MathCeil				= math.ceil
local g_MathFloor				= math.floor
local g_MathMax					= math.max
local g_MathMin					= math.min

--=================================================================================================================
--=================================================================================================================
						--PRACTICAL FUNCTIONS (Eg: Calculations, random, optimizations, utils)
--=================================================================================================================
--=================================================================================================================

-- GetRandom number
function GetRandom(lower, upper)
    return Game.Rand((upper + 1) - lower, "") + lower
end

--Game_GetRound (from JFD)
function Game_GetRound(num, idp)
    local mult = 10^(idp or 0)
    return g_MathFloor(num * mult + 0.5) / mult
end
local g_GetRound = Game_GetRound


function JFD_GetNumDomesticRoutesFromThisCity(player, city) -- for both Sea and Land
    local tradeRoutes = player:GetTradeRoutes()
    local numDomesticRoutes = 0
    for tradeRouteID, tradeRoute in ipairs(tradeRoutes) do
        local domain = tradeRoute.Domain
        local originatingCity = tradeRoute.FromCity
        local targetCity = tradeRoute.ToCity
        if targetCity and originatingCity == city then
            numDomesticRoutes = numDomesticRoutes + 1
        end
    end
return numDomesticRoutes
end

local iPracticalNumCivs = (GameDefines.MAX_MAJOR_CIVS - 1)

function JFD_IsCivilisationActive(civilizationID)
	for iSlot = 0, iPracticalNumCivs, 1 do
		local slotStatus = PreGame.GetSlotStatus(iSlot)
		if (slotStatus == SlotStatus["SS_TAKEN"] or slotStatus == SlotStatus["SS_COMPUTER"]) then
			if PreGame.GetCivilization(iSlot) == civilizationID then
				return true
			end
		end
	end
	return false
end

function array_has_value(tab, val)
	for index, value in ipairs(tab) do
			if value == val then
					return true
			end
	end
	return false
end

local NATIONAL_WONDER_CLASSES = {
	'BUILDINGCLASS_HEROIC_EPIC',
	'BUILDINGCLASS_NATIONAL_EPIC',
	'BUILDINGCLASS_CIRCUS_MAXIMUS',
	'BUILDINGCLASS_NATIONAL_TREASURY',
	'BUILDINGCLASS_NATIONAL_COLLEGE',
	'BUILDINGCLASS_GRAND_TEMPLE',
	'BUILDINGCLASS_IRONWORKS',
	'BUILDINGCLASS_OXFORD_UNIVERSITY',
	'BUILDINGCLASS_HERMITAGE',
	'BUILDINGCLASS_INTELLIGENCE_AGENCY',
	'BUILDINGCLASS_TOURIST_CENTER'
}

function isNationalWonder(buildingID)
	local buildingClass = GameInfo.Buildings[buildingID].BuildingClass
	local isNational = array_has_value(NATIONAL_WONDER_CLASSES, buildingClass)
	return isNational
end

function countNationalWondersInCity(city)
	local numNationalWonders = 0
	-- This bugs currently on hermitage
	-- for _, wonderClass in ipairs(NATIONAL_WONDER_CLASSES) do
	-- 	if city:IsHasBuilding(GameInfoTypes[wonderClass]) then
	-- 		print("Found National Wonder: " .. wonderClass)
	-- 		numNationalWonders = numNationalWonders + 1
	-- 	end
	-- end
	for building in GameInfo.Buildings() do
		if (city:IsHasBuilding(building.ID) and isNationalWonder(building.ID)) then
			print("Found National Wonder: " .. wonderClass)
			numNationalWonders = numNationalWonders + 1
		end
	end
	return numNationalWonders
end
