-- nuke range promos
-- author: Ashwin
-- date: feb 18 2025

include("snowveutilities.lua")

local nukeID = GameInfoTypes["UNITCOMBAT_NUKE"]
local rangePromotionID = GameInfoTypes["PROMOTION_NUKE_THROW"]
local metalRangePromotionID = GameInfoTypes["PROMOTION_METAL_NUKE_THROW"]
local robotID = GameInfoTypes["UNIT_MECH"]
local metalID = GameInfoTypes["UNIT_METAL_GEAR"]

function nukeRangePromos(iPlayerID, iUnitID, iX, iY)
    local pPlayer = Players[iPlayerID]
    local pUnit = pPlayer:GetUnitByID(iUnitID)

    local isNuke = (pUnit:GetUnitCombatType() == nukeID)
    if isNuke then 
        local pPlot = Map.GetPlot(iX, iY)
        local bGavePromo = false
        -- should we get a nuke throw bonus?
        -- first, set the promos to false

        -- check if there's a city there
        if pPlot:GetPlotCity() ~= nil then
            pUnit:SetHasPromotion(rangePromotionID, true)
            bGavePromo = true
        -- if not, check if there's a gdr on the tile
        else
            for i = 0, pPlot:GetNumUnits() - 1, 1 do
                local pPlotUnit = pPlot:GetUnit(i)
                if pPlotUnit:GetUnitType() == robotID then
                    pUnit:SetHasPromotion(rangePromotionID, true)
                    bGavePromo = true
                    break
                elseif pPlotUnit:GetUnitType() == metalID then
                    pUnit:SetHasPromotion(rangePromotionID, true)
                    pUnit:SetHasPromotion(metalRangePromotionID, true)
                    bGavePromo = true
                    break
                end
            end
        end
        if (not bGavePromo) then
            pUnit:SetHasPromotion(rangePromotionID, false)
            pUnit:SetHasPromotion(metalRangePromotionID, false)
        end
    end
end

GameEvents.RebaseTo.Add(nukeRangePromos)