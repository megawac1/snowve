-- new zealand border expansion
-- thanks to Konrad for implementing this effect in skirm so I could steal it

include("snowveutilities.lua")
include("PlotIterators.lua")
include("FLuaVector")

local iCiv = GameInfoTypes["CIVILIZATION_MC_NEW_ZEALAND"]
local bIsActive = JFD_IsCivilisationActive(iCiv)

local g_MapGetPlot		= Map.GetPlot
local horseTech = GameInfoTypes["TECH_ANIMAL_HUSBANDRY"]


local function NZPlayerCityFounded(playerID, plotX, plotY)
    local player = Players[playerID]
    local playerIsHuman = player:IsHuman()

    if (player:IsAlive() and player:GetCivilizationType() == iCiv) then
        local plot = g_MapGetPlot(plotX, plotY)
        local city = plot:GetPlotCity()
        local cityID = city:GetID()
        local bTeamHorseTeched = Teams[player:GetTeam()]:IsHasTech(horseTech)
        for adjacentPlot in PlotAreaSweepIterator(plot, 2, SECTOR_NORTH, DIRECTION_CLOCKWISE, DIRECTION_OUTWARDS, CENTRE_EXCLUDE) do
            if adjacentPlot:GetOwner() == -1 then
                local resourceID = adjacentPlot:GetResourceType()
                if (resourceID == 7 or resourceID == 8 or (resourceID == 1 and bTeamHorseTeched)) then
                    adjacentPlot:SetOwner(playerID)
                    adjacentPlot:SetCityPurchaseID(cityID)
                    if playerIsHuman then
                        local hex = ToHexFromGrid(Vector2(adjacentPlot:GetX(), adjacentPlot:GetY()))
                        Events.GameplayFX(hex.x, hex.y, -1)
                    end
                end
            end
        end
    end
end

if bIsActive then
    GameEvents.PlayerCityFounded.Add(NZPlayerCityFounded)
end
