include("snowveutilities.lua")


-- krivis
-- Author: ashwin
-- DateCreated: 12/17/2024
--------------------------------------------------------------

local iInquisitorID = GameInfoTypes["UNIT_INQUISITOR"]
local iCiv = GameInfoTypes["CIVILIZATION_LITHUANIA"]
local bIsActive = JFD_IsCivilisationActive(iCiv)
local medic1 = GameInfoTypes["PROMOTION_MEDIC"]
local medic2 = GameInfoTypes["PROMOTION_MEDIC_II"]

function LithuaniaInquisitorPromotion(iPlayer, iUnit)
    local pPlayer = Players[iPlayer];
    if (pPlayer:IsEverAlive()) then
        if (pPlayer:GetCivilizationType() == iCiv) then
       	    if pPlayer:GetUnitByID(iUnit) ~= nil then
		        pUnit = pPlayer:GetUnitByID(iUnit);
                if (pUnit:GetUnitType() == iInquisitorID) then
                    pUnit:SetHasPromotion(medic1, true)
                    pUnit:SetHasPromotion(medic2, true)
                end
            end
        end
    end
end



--Spawn a free Krivis at Philosophy
local krivisTechs = {
    GameInfoTypes["TECH_ENGINEERING"],
}

local krivisBuildingPolicy = GameInfoTypes["BUILDING_KRIVIS_POLICY"]
local krivisBuildingNational = GameInfoTypes["BUILDING_KRIVIS_NATIONAL"]

function LithuaniaKrivisSpawnTech(iTeam, iTech, bAdopted)
    for playerID, player in pairs(Players) do
        if player:GetCivilizationType() == iCiv then
            if player:GetTeam() == iTeam then
                if array_has_value(krivisTechs, iTech) then
                    local pCity = player:GetCapitalCity();
                    iNumToSet = pCity:GetNumBuilding(krivisBuildingPolicy) + 1
                    pCity:SetNumRealBuilding(krivisBuildingPolicy, iNumToSet)
                end
            end
        end
    end
end

-- spawn a free krivis upon completion of a social policy tree
function LithuaniaKrivisSpawnPolicy(playerID, policyID)
    local player = Players[playerID]
    local policy = GameInfo.Policies[policyID]
    local policy_branch = GameInfo.PolicyBranchTypes[policy.PolicyBranchType]

    if player:IsEverAlive() then
        if player:GetCivilizationType() == iCiv then
            if policy_branch ~= nil and player:IsPolicyBranchFinished(policy_branch.ID) then
                local pCity = player:GetCapitalCity()
                iNumToSet = pCity:GetNumBuilding(krivisBuildingPolicy) + 1
                pCity:SetNumRealBuilding(krivisBuildingPolicy, iNumToSet)
            end
        end
    end
end

function LithuaniaNationalBuildingBuiltSpawn(playerID, cityID, buildingID)
    local player = Players[playerID]
    if player:GetCivilizationType() == iCiv then
        local city = player:GetCityByID(cityID)
        if isNationalWonder(buildingID) then
            -- Max 1 krivis per city
            city:SetNumRealBuilding(krivisBuildingNational, 1)
        end
    end
end

if bIsActive then
	Events.SerialEventUnitCreated.Add(LithuaniaInquisitorPromotion)
    GameEvents.TeamSetHasTech.Add(LithuaniaKrivisSpawnTech)
    GameEvents.PlayerAdoptPolicy.Add(LithuaniaKrivisSpawnPolicy)
    GameEvents.CityConstructed.Add(LithuaniaNationalBuildingBuiltSpawn)
end