-- indonesia extra luxuries
-- author: Ashwin
-- created: 12/17/2024

include("snowveutilities.lua")

local iCiv = GameInfoTypes["CIVILIZATION_INDONESIA"]
local clovesBuilding = GameInfoTypes["BUILDING_DUMMY_CLOVES"]
local nutmegBuilding = GameInfoTypes["BUILDING_DUMMY_NUTMEG"]
local pepperBuilding = GameInfoTypes["BUILDING_DUMMY_PEPPER"]
local foodBuilding = GameInfoTypes["BUILDING_DUMMY_FOOD"]

local nationalEpicID = GameInfoTypes["BUILDING_NATIONAL_EPIC"]
local nationalCollegeID = GameInfoTypes["BUILDING_NATIONAL_COLLEGE"]
local eastIndiaID = GameInfoTypes["BUILDING_NATIONAL_TREASURY"]

local foodFromNW = 3

function indonesiaLuxurySpawn(playerID, cityID, buildingID)
    local player = Players[playerID]
    if player:GetCivilizationType() == iCiv then 
        if buildingID == nationalEpicID then
            local city = player:GetCityByID(cityID)
            city:SetNumRealBuilding(clovesBuilding, 1)
            local iNumToSet = city:GetNumBuilding(foodBuilding) + foodfromNW
            city:SetNumRealBuilding(foodBuilding, iNumToSet)
        elseif buildingID == nationalCollegeID then
            local city = player:GetCityByID(cityID)
            city:SetNumRealBuilding(nutmegBuilding, 1)
            local iNumToSet = city:GetNumBuilding(foodBuilding) + foodfromNW
            city:SetNumRealBuilding(foodBuilding, iNumToSet)
        elseif buildingID == eastIndiaID then
            local city = player:GetCityByID(cityID)
            city:SetNumRealBuilding(pepperBuilding, 1)
            local iNumToSet = city:GetNumBuilding(foodBuilding) + foodfromNW
            city:SetNumRealBuilding(foodBuilding, iNumToSet)
        end
    end
end

local bIsActive = JFD_IsCivilisationActive(iCiv)

if bIsActive then
    GameEvents.CityConstructed.Add(indonesiaLuxurySpawn)
end